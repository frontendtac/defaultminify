# Default Minify [CSS + JS]

Padrão de estrutura para minificação utilizando GULP

## To Start/Run

* $ clone: [git@bitbucket.org:frontendtac/defaultminify.git](git@bitbucket.org:frontendtac/defaultminify.git)
* $ npm install
* $ gulp

### O index.js fica em /src/js 
### O style.css fica em /src/scss (na estrutura que está ele deve virar um scss para buildar)

### Tecnologias utilizadas:

* GULP
* SASS
* NODEJS


Estrutura de pastas baseada no Fastshell
* Source: [github.com/HosseinKarami/fastshell](http://github.com/HosseinKarami/fastshell)
* Documentation: [DOCS.md](https://github.com/HosseinKarami/fastshell/blob/master/DOCS.md)
* HomePage: [Fastshell](https://HosseinKarami.github.io/fastshell)