///HTML5 - IE < 9
/*
 HTML5 Shiv v3.7.0 | @afarkas @jdalton @jon_neal @rem | MIT/GPL2 Licensed
*/
(function(l, f) {
    function m() {
        var a = e.elements;
        return "string" == typeof a ? a.split(" ") : a
    }

    function i(a) {
        var b = n[a[o]];
        b || (b = {}, h++, a[o] = h, n[h] = b);
        return b
    }

    function p(a, b, c) {
        b || (b = f);
        if (g) return b.createElement(a);
        c || (c = i(b));
        b = c.cache[a] ? c.cache[a].cloneNode() : r.test(a) ? (c.cache[a] = c.createElem(a)).cloneNode() : c.createElem(a);
        return b.canHaveChildren && !s.test(a) ? c.frag.appendChild(b) : b
    }

    function t(a, b) {
        if (!b.cache) b.cache = {}, b.createElem = a.createElement, b.createFrag = a.createDocumentFragment, b.frag = b.createFrag();
        a.createElement = function(c) {
            return !e.shivMethods ? b.createElem(c) : p(c, a, b)
        };
        a.createDocumentFragment = Function("h,f", "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" + m().join().replace(/[\w\-]+/g, function(a) {
            b.createElem(a);
            b.frag.createElement(a);
            return 'c("' + a + '")'
        }) + ");return n}")(e, b.frag)
    }

    function q(a) {
        a || (a = f);
        var b = i(a);
        if (e.shivCSS && !j && !b.hasCSS) {
            var c, d = a;
            c = d.createElement("p");
            d = d.getElementsByTagName("head")[0] || d.documentElement;
            c.innerHTML = "x<style>article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}</style>";
            c = d.insertBefore(c.lastChild, d.firstChild);
            b.hasCSS = !!c
        }
        g || t(a, b);
        return a
    }
    var k = l.html5 || {},
        s = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,
        r = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,
        j, o = "_html5shiv",
        h = 0,
        n = {},
        g;
    (function() {
        try {
            var a = f.createElement("a");
            a.innerHTML = "<xyz></xyz>";
            j = "hidden" in a;
            var b;
            if (!(b = 1 == a.childNodes.length)) {
                f.createElement("a");
                var c = f.createDocumentFragment();
                b = "undefined" == typeof c.cloneNode ||
                    "undefined" == typeof c.createDocumentFragment || "undefined" == typeof c.createElement
            }
            g = b
        } catch (d) {
            g = j = !0
        }
    })();
    var e = {
        elements: k.elements || "abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",
        version: "3.7.0",
        shivCSS: !1 !== k.shivCSS,
        supportsUnknownElements: g,
        shivMethods: !1 !== k.shivMethods,
        type: "default",
        shivDocument: q,
        createElement: p,
        createDocumentFragment: function(a, b) {
            a || (a = f);
            if (g) return a.createDocumentFragment();
            for (var b = b || i(a), c = b.frag.cloneNode(), d = 0, e = m(), h = e.length; d < h; d++) c.createElement(e[d]);
            return c
        }
    };
    l.html5 = e;
    q(f)
})(this, document);

! function(e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : e("object" == typeof exports ? require("jquery") : jQuery)
}(function(e) {
    function n(e) {
        return u.raw ? e : encodeURIComponent(e)
    }

    function o(e) {
        return u.raw ? e : decodeURIComponent(e)
    }

    function i(e) {
        return n(u.json ? JSON.stringify(e) : String(e))
    }

    function r(e) {
        0 === e.indexOf('"') && (e = e.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, "\\"));
        try {
            return e = decodeURIComponent(e.replace(c, " ")), u.json ? JSON.parse(e) : e
        } catch (n) {}
    }

    function t(n, o) {
        var i = u.raw ? n : r(n);
        return e.isFunction(o) ? o(i) : i
    }
    var c = /\+/g,
        u = e.cookie = function(r, c, a) {
            if (arguments.length > 1 && !e.isFunction(c)) {
                if (a = e.extend({}, u.defaults, a), "number" == typeof a.expires) {
                    var f = a.expires,
                        s = a.expires = new Date;
                    s.setTime(+s + 864e5 * f)
                }
                return document.cookie = [n(r), "=", i(c), a.expires ? "; expires=" + a.expires.toUTCString() : "", a.path ? "; path=" + a.path : "", a.domain ? "; domain=" + a.domain : "", a.secure ? "; secure" : ""].join("")
            }
            for (var d = r ? void 0 : {}, p = document.cookie ? document.cookie.split("; ") : [], m = 0, x = p.length; x > m; m++) {
                var l = p[m].split("="),
                    g = o(l.shift()),
                    k = l.join("=");
                if (r && r === g) {
                    d = t(k, c);
                    break
                }
                r || void 0 === (k = t(k)) || (d[g] = k)
            }
            return d
        };
    u.defaults = {}, e.removeCookie = function(n, o) {
        return void 0 === e.cookie(n) ? !1 : (e.cookie(n, "", e.extend({}, o, {
            expires: -1
        })), !e.cookie(n))
    }
});
/* v2*/


var geral = {
    newsLb: function() {
        $("#newsPopUp .close, .newsPopUpBg").bind("click", function() {
            $("#newsPopUp").fadeOut("slow", function() {
                $("#newsPopUp").remove();
            })
        });

        if (typeof $.cookie("newsToque2") == "undefined") {
            $("#newsPopUp").fadeIn("slow");
            $.cookie("newsToque2", "1", {
                expires: 365
            });
        }
    },
    tabs: function() {
        $(".tabWrap > ul li").on("click", "a", function(event) {
            event.preventDefault();
            $tab = $(this);
            $tab.parents('.tabWrap').children("ul").children('li').removeClass("active");
            $tab.closest("li").addClass("active");
            var href = $tab.attr("href");
            $tab.parents('.tabWrap').find('div').removeClass("active");
            $tab.parents('.tabWrap').find(href).addClass("active");

        });
    },
    vitrine: function() {
        $(".helperComplement").remove();
        $('.prateleira li').each(function() {
            //$( $(this).find('.btn-quickview') ).after( $(this).find('.see-more').clone() );
            //$(this).find('.see-more').eq(1).remove();

            if ($(this).find('.product-field').length === 0) {
                $(this).find('.dimensions').remove();
            }

            var blackfriday = $(this).find('.flag.black-friday').html();
            if (blackfriday != undefined) {
                $(this).children('.productImage').append('<div class="blackfriday-esquenta"><img src="/arquivos/flags-blackfriday.png" alt="blackfriday" title="blackfriday"></div>');
            }


        });
        $(document).ajaxStop(function() {

            $('.prateleira li').each(function() {
                //$( $(this).find('.btn-quickview') ).after( $(this).find('.see-more').clone() );
                //$(this).find('.see-more').eq(1).remove();

                if ($(this).children('.productImage').children('.blackfriday-esquenta').length == 0) {
                    var blackfriday = $(this).find('.flag.black-friday').html();

                    if (blackfriday != undefined) {
                        $(this).children('.productImage').append('<div class="blackfriday-esquenta"><img src="/arquivos/flags-blackfriday.png" alt="blackfriday" title="blackfriday"></div>');
                    }
                }
            });
        });
    },
    thumbsSimilares: function() {
        var slickcss = document.createElement('link');
        slickcss.src = "//toqueacampainha.vteximg.com.br/arquivos/slick.css";

        var slickjs = document.createElement('script');
        slickjs.src = "//toqueacampainha.vteximg.com.br/arquivos/slick.min.js";

        document.head.appendChild(slickcss);
        document.head.appendChild(slickjs);

        $('.resultItemsWrapper > .prateleira > .prateleira > ul > li').hover(function() {
            //var texto = 'http://toqueacampainha.vtexcommercestable.com.br/puff-britto-patchwork-pe-castanho-revestimento-patchwork/p';
            var $this = $(this);
            var texto = $this.find('a').eq(0).attr('href');
            texto = texto.split('/');
            var urli = "";

            if ($this.find('.related').length == 0) {
                for (var i = 0; i < texto.length; i++) {
                    if (i > 2) { urli += '/' + texto[i]; }

                    if (i == (texto.length - 1)) {
                        $.ajax({
                            headers: {
                                'Content-Type': 'application/json; charset=utf-8',
                            },
                            crossDomain: true,
                            url: urli,
                            data: { lid: 'e53760c0-57c7-4b1b-93cd-107a90a91c21' },
                            dataType: 'html',
                            beforeSend: function() {
                                $this.find('.data').eq(0).addClass('loading');
                            },
                            success: function(data) {
                                /* existem produtos que não possuem produtos relacionados */
                                var related = $(data).find('.productRelated').find('ul').html();
                                if ($this.find('.related').length == 0) {
                                    $this.find('.data').eq(0).append('<div class="related"><ul></ul></div>');
                                    var rel = $this.find('.related ul').append(related);
                                    $('.related .data').remove();
                                    $('.related .helperComplement').remove();

                                    $(rel).slick({
                                        slide: 'li',
                                        infinite: false,
                                        slidesToShow: 3,
                                        slidesToScroll: 1,
                                        autoplay: true,
                                        autoplaySpeed: 2000
                                    });
                                }
                                $this.find('.data').removeClass('loading');

                            }
                        });
                    }
                }
            }
        });
    },
    menu: function() {
        // cor dos menus
        $('#menu .menu-item').mouseover(function() {
            $('#menu .menu-item').addClass('inative');
            $(this).addClass('active').removeClass('inative');

        });
        $('#menu .menu-item').mouseout(function() {
            $('#menu .menu-item').removeClass('inative');
            $('#menu .menu-item').removeClass('active');
        });

        // inserir banners
        var menuCategory = $('#menu .menu-item');
        var menuWrapper = $('#menu .submenu .wrapper');
        menuWrapper.append('<div class="bannerCategory"></div>');

        menuCategory.each(function() {
            var idCategory = $(this).find('h2').attr('class');
            $(this).find('.bannerCategory').load('/bannerMenu #' + idCategory);
        })
    },
    topBar: function() {
        var floatingBar = $(".float-bar");
        var smartCart = $("header .cart");
        var logoStore = $("header .logo");
        var searchBox = $("#menu-bar .search");
        var autoComplete = $(".ui-autocomplete");
        $(window).bind("scroll", function() {
            if ($(this).scrollTop() > 100) {
                logoStore.addClass("floating");
                searchBox.addClass("floating");
                autoComplete.addClass("floating");
                smartCart.addClass("floatingCart");
                floatingBar.addClass("active");
            } else {
                logoStore.removeClass("floating");
                searchBox.removeClass("floating");
                autoComplete.removeClass("floating");
                smartCart.removeClass("floatingCart");
                floatingBar.removeClass("active");
            }
        });
    },
    getUrlParameter: function(sParam, sPageURL) {
        sPageURL = sPageURL != null ? sPageURL : decodeURIComponent(window.location.search.substring(1));
        var sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;


        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    },
    createCookie: function() {
        if (location.search.indexOf('utm_source') >= 0) {
            var param = geral.getUrlParameter('utm_source', null);
            $.cookie(param, param);
        }
    },
    call: function() {
        geral.newsLb();
        geral.vitrine();
        geral.tabs();
        geral.menu();
        geral.topBar();
        geral.createCookie();
        geral.thumbsSimilares();
    }
};
var product = {
    boxs: function() {
        $(".box .title").on("click", function() {
            $(this).closest("div").find(".box-content").slideToggle("slow");
        });

        var idProd = skuJson_0.skus[0].sku;
        //console.log(idProd);

        /*$.ajax({
            url:"/productotherpaymentsystems/"+idProd,
            async:false
        }).done(function(data) {
            $(".other-payment-method").html(data);
        });
        */

        $(".other-payment-method").load("/productotherpaymentsystems/" + idProd + " .contentWrapper", function() {
            $("#ddlCartao").change(function() {
                var selected = $(this).val();
                if (selected == "")
                    selected = "1";

                var name = $("#ddlCartao option:selected").text();

                $("#card-flag").html(name);
                $("#card-flag").attr("class", "card-flag " + name);
                switch (name) {
                    case "American Express":
                        $("#card-flag").css("background-position", "-120px 0");
                        break;
                    case "Visa":
                        $("#card-flag").css("background-position", "0 0");
                        break;
                    case "Diners":
                        $("#card-flag").css("background-position", "-80px 0");
                        break;
                    case "Mastercard":
                        $("#card-flag").css("background-position", "-40px 0");
                        break;
                    case "Discover":
                        $("#card-flag").css("background-position", "-200px 0");
                        break;
                    case "Aura":
                        $("#card-flag").css("background-position", "-280px 0");
                        break;
                    case "Hipercard":
                        $("#card-flag").css("background-position", "-160px 0");
                        break;
                    case "Elo":
                        $("#card-flag").css("background-position", "-320px 0");
                        break;
                    case "Banricompras":
                        $("#card-flag").css("background-position", "-240px 0");
                        break;
                    default:
                        $("#card-flag").attr("background-position", "");
                        break;
                }

                $(".tbl-payment-system").hide();
                //$("#tbl" + selected).show();
                $("#tbl" + selected).css('display', 'flex');
            });

            //load screen with default values
            $("#ddlCartao").change();
        });


    },
    boleto: function() {

        var porcentoDesconto = 0;
        if ($('.flags-hightlight').find('[class*="desconto-"]').length > 0) {
            porcentoDesconto = $('[class*="desconto-"]').text().match(/\d/g);
            porcentoDesconto = porcentoDesconto.join("");
        }
        pClub = $(".skuBestPrice").text();

        var pClubrNum = pClub.replace('R$ ', '').replace(".", "");

        var pClubrNumReal = parseFloat(pClubrNum.replace(".", "").replace(",", "."));
        var pClubrNumReal = pClubrNumReal - (pClubrNumReal * porcentoDesconto) / 100;
        var pClubrNumReal = pClubrNumReal.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();

        var pClubrNumStr = pClubrNumReal.replace(',', '.').split('.');
        var pClubrNumString = '';
        var v = '';
        var pLen = (pClubrNumStr.length) - 2;
        $.each(pClubrNumStr, function(index, val) {
            v = index < pLen ? '.' : index == pLen ? ',' : '';
            pClubrNumString += val + v;
        });

        $("#productPrice .boleto p b").html('R$ ' + pClubrNumString);

        $("#productPrice .boleto p .porcento-desconto").html(porcentoDesconto);
        //$("#productPrice .boleto p .de-desconto").html('% de desconto');
        $("#productPrice .boleto").show();
        if (porcentoDesconto == 0) {
            $('#productPrice > .boleto').hide();
        }

        $("#ltlPrecoWrapper.valor em").html('R$ ' + pClubrNumString);
    },

    list: function() {
        $(".glis-popup-link-add, .glis-popup-link").text("ADICIONAR Á LISTA DE DESEJOS");
    },

    parcelamento: function() {
        $('table.tbl-payment-system td.parcelas').each(function() {
            var newText = $(this).text().replace(/[^0-9.]/g, "");
            if ($(this).text().indexOf("Ã€ vista") > -1) {
                $(this).text("Ã€ vista");
            } else {
                $(this).text(newText);
                $(this).append('X S/JUROS');
            }
        })
    },

    acessorios: function() {
        $("#acessoriosShelf .box-preco-atualizado").append('<a class="buy-acessorios" style="cursor:pointer">Comprar</a>');
        var btnAdicionar = $("#acessoriosShelf .prateleira ul").next("fieldset");
        //console.log(btnAdicionar);

        if ($("#acessoriosShelf .prateleira ul").length) {
            $("#acessoriosShelf ul li").each(function() {
                if ($(this).next("fieldset").length) {
                    $(this).next("fieldset").appendTo($(this));
                } else {
                    //$("#acessoriosShelf .prateleira ul").next("fieldset").appendTo($(this));
                    $(btnAdicionar).appendTo("#acessoriosShelf ul li.last");
                }
            });


        } else {
            $("#acessoriosShelf").remove();
        }
    },

    flags: function() {
        if ($('.flags-hightlight').find('.flag.pronta-entrega').length > 0) {
            $('#mainContentSide h1').after($('.flag.pronta-entrega').clone());
        }
    },

    thumbs: function() {
        if ($('.value-field.Video').length > 0) {
            $('.value-field.Video').parent().addClass('ignore').hide();

            product.modalVideo();
            $('ul.thumbs').prepend('<li class="thumb-video"><img src="/arquivos/thumb-video.png" alt="Video" title="Video" /></li>');
            $('ul.thumbs li').live('click', function() {
                if ($(this).hasClass('thumb-video')) {
                    $('ul.thumbs li').removeClass('slick-active');
                    var video = $('.value-field.Video').children('iframe').attr('src');

                    $('.modalWrap').html('<div id="video"><iframe width="800px" height="310px" src="' + video + '"></iframe></div>');
                    $(this).addClass('slick-active');
                    product.modalVideoExibir();
                    $('#image').hide();
                    $('.overlayVideo, .info-close').on('click', function() {
                        product.modalVideoOcultar();
                        $('#video').remove();
                        $('#image').show();
                    });

                } else {
                    $('#video').remove();
                    $('#image').show();
                }
            });

        }
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? false : sParameterName[1];
                }
            }

        };

        var dev = getUrlParameter('lid');

        if (dev == '78a49edb-0456-4d43-85a2-59d561fcef4d' || dev == '233064d1-59b5-4206-a6e6-dfffeb1fdf02') {

        } else {
            if ($('ul.thumbs li').length > 4) {
                $('ul.thumbs').slick({
                    slidesToShow: 4,
                    slidesToScroll: 4
                });
            }
        }

        $(document).ajaxStop(function() {
            var j = 0;
            $.each($('.productDescription table tr'), function(ind, elem) {
                if (!$(elem).hasClass('ignore')) {
                    if (j % 2 == 0) {
                        $(elem).addClass('even');
                    } else {
                        $(elem).removeClass('even');
                    }
                    j++;
                }
            });
        });

    },

    modalVideo: function() {
        var modalHTML = '<div class="overlayVideo"></div><div class="modalVideo"><div class="info-close"><a href="#" title="Fechar">Fechar</a></div><h3></h3><div class="modalWrap"></div></div>';
        $('body').append(modalHTML);
    },

    modalVideoExibir: function() {
        $('.overlayVideo').fadeIn(400);
        $('.modalVideo').fadeIn(400);
    },

    modalVideoOcultar: function() {
        $('.overlayVideo').fadeOut(400);
        $('.modalVideo').fadeOut(400);
    },

    indisponivel: function() {
        if ($('#BuyButton .buy-button').css('display') === 'none') {
            $('body').addClass('produtoIndisponivel');
            $('.btn-ok').attr('value', 'Avise-me');
        };
    },

    specialSpecs: function() {
        var c = false;
        if ($('table.group.Caracteristicas').find('.Info-Extra').length > 0) {
            var extra = $('.value-field.Info-Extra').html();
            $('table.group.Caracteristicas').find('.Info-Extra').remove();
            $('section.description').after(extra);
            c = true;
        }
        if ($('table.group.Especificacoes').find('.value-field.Manual-de-Montagem').length > 0) {
            var link = $('table.group.Especificacoes').find('.value-field.Manual-de-Montagem').html();
            var manual = '<div class="manual">' +
                '<a href="' + link + '" target="_blank">' +
                '<i class="baixar-manual"></i> <span class="baixar-manual-texto"></span>' +
                '</a>' +
                '</div>';
            $('table.group.Especificacoes').find('.value-field.Manual-de-Montagem').html(manual);
            $('#mainContentSide').append(manual);
        }

        $('.productDescription').append('<table cellspacing="0"><tbody></tbody></table>');
        var specToSpecial = [
            "Macio-ou-Firme-",
            "Tamanho",
            "Design",
            "Com-o-tecido-certo",
            "Praticidade",
            "Para-que-",
            "Tampo",
            "Use-tambem-para",
            "Onde-usar-",
            "Assento",
            "Encosto",
            "Utilidades",
            "Somos-um-casal",
            "O-que-tem-",
        ];
        if ($('.spec table').find('th').length > 0) {
            $(".spec table th, .spec table td").each(function() {
                _this = $(this);
                _this.addClass("specNormal")
                $.each(specToSpecial, function(i) {
                    if (_this.attr("class").indexOf(specToSpecial[i]) > 0) {
                        _this.addClass("specSpecial").removeClass("specNormal");
                    }
                });
                $.each($('.specNormal'), function() {
                    $('.productDescription table tbody').append($('.specNormal').parent());
                })
            });
        } else if ($('.spec').find('dd').length > 0) {
            $('.productDescription').html($('dt').text());
            $('.productDescription').append('<table><tbody></tbody></table>');
            $.each($(".spec dd"), function(i, v) {
                _this = $(this);
                var tb = _this.text().split(': ');
                var even = i % 2 == 0 ? 'class="even"' : '';
                var table = "<tr " + even + ">";
                $.each(tb, function(ind, val) {
                    if (ind == 0) {
                        table += '<th class="name-field">' + val + '</th>';
                    } else {
                        table += '<td class="value-field">' + val + '</td>';
                    }
                });
                table += "</tr>";

                $('.productDescription table tbody').append(table);

            });
        }


        if ($('.spec table tr').length == 0) {
            //$('.spec').remove();
        } else if ($('table.group.Caracteristicas').find('tr').length == 1 && c) {
            //$('table.group.Caracteristicas').parents('.spec').hide();
        }
    },

    modal: function() {
        $('.box.frete .box-content').append("<p class='montagem' style='display:none'>Montagem: <strong>grátis</strong></p>");
        var modalHTML = '<div class="overlayMostruario"></div><div class="modalMostruario"><div class="info-close"><a href="#" title="Fechar">Fechar</a></div><h3></h3><div class="modalWrap"></div></div>';
        $('body').append(modalHTML);
    },

    modalExibir: function() {
        $('.overlayMostruario').fadeIn(400);
        $('.modalMostruario').fadeIn(400);
    },

    modalOcultar: function() {
        $('.overlayMostruario').fadeOut(400);
        $('.modalMostruario').fadeOut(400);
    },

    mostruario: function() {

        $('.modalMostruario h3').html("Este produto está em mostruário nas lojas abaixo:");

        if ($('th.Produto-Exposicao').length == 0) {
            $('.grayBox.mostruario').css('display', 'none');
        } else {
            $(this).parent("tr").css('display', 'none');


            var jsonString = $('.jsonTag').text();
            var obj = jQuery.parseJSON(jsonString);
            aryLojas = [];
            for (var i = 0; i < obj.length; i++) {
                var idLoja = obj[i].idLoja;
                var nomeLoja = obj[i].loja;
                var endereco = obj[i].endereco;
                var bairro = obj[i].bairro;
                var cep = obj[i].cep;
                var municipio = obj[i].municipio;
                var telefone = obj[i].telefone;
                var qtd = obj[i].quantidade;

                var strHTML = "<div class='lojaItem'>" +
                    "<h5>" + nomeLoja + "</h5>" +
                    "<p><b>End.: </b>" + endereco + "</p>" +
                    "<p><b>Bairro.: </b>" + bairro + " <b>CEP:</b>" + cep + "</p>" +
                    "<p><b>Município: </b>" + municipio + "</p>" +
                    "<p><b>Fone: </b>" + telefone + "</p>" +
                    "<p><b>Quantidade em estoque: </b>" + qtd + "</p>" +
                    "</div>";
                aryLojas.push(strHTML);

            };
            $('.modalMostruario .modalWrap').html(aryLojas);
            $('td.Produto-Exposicao').html(aryLojas);

        };
    },

    mostruarioTable: function() {

        $('.modalMostruario h3').html("Este produto está em mostruário nas lojas abaixo:");



        if ($('th.Produto-Exposicao').length == 0) {
            $('.grayBox.mostruario').css('display', 'none');
        } else {
            $(this).parent("tr").css('display', 'none');



            var jsonString = $('td.Produto-Exposicao').text();
            var obj = jQuery.parseJSON(jsonString);
            var jsonTag = '<div class="jsonTag">' + jsonString + '</div>';
            $(jsonTag).appendTo('body');
            aryLojas = [];
            for (var i = 0; i < obj.length; i++) {
                var idLoja = obj[i].idLoja;
                var nomeLoja = obj[i].loja;
                var endereco = obj[i].endereco;
                var bairro = obj[i].bairro;
                var cep = obj[i].cep;
                var municipio = obj[i].municipio;
                var telefone = obj[i].telefone;
                var qtd = obj[i].quantidade;

                var strHTML = "<div class='lojaItem'>" +
                    "<h5>" + nomeLoja + "</h5>" +
                    "<p><b>End.: </b>" + endereco + "</p>" +
                    "<p><b>Bairro.: </b>" + bairro + " <b>CEP:</b>" + cep + "</p>" +
                    "<p><b>Município: </b>" + municipio + "</p>" +
                    "<p><b>Fone: </b>" + telefone + "</p>" +
                    "<p><b>Quantidade em estoque: </b>" + qtd + "</p>" +
                    "</div>";
                aryLojas.push(strHTML);

            };
            $('.modalMostruario .modalWrap').html(aryLojas);
            $('td.Produto-Exposicao').html(aryLojas);

        };
    },

    store: function() {
        if ($('td.Produto-Exposicao').length == 0) {
            $(this).parent("tr").css('display', 'none');
        } else {
            var jsonStringStore = $('td.Produto-Exposicao').text();
            var objStore = jQuery.parseJSON(jsonStringStore);
            arrayLojas = [];
            for (var i = 0; i < objStore.length; i++) {
                var idLoja = objStore[i].idLoja;
                var nomeLoja = objStore[i].loja;
                var endereco = objStore[i].endereco;
                var bairro = objStore[i].bairro;
                var cep = objStore[i].cep;
                var municipio = objStore[i].municipio;
                var telefone = objStore[i].telefone;
                var qtd = objStore[i].quantidade;

                var strHTML = "<h5>" + nomeLoja + "</h5>";
                arrayLojas.push(strHTML);

            };

            $('td.Produto-Exposicao').html(arrayLojas);
        }

    },

    montagemGratis: function() {
        var html = '<div class="institucional-content"><p>A Toque a Campainha realiza o serviço de montagem somente no município do Rio de Janeiro, num range de CEPs pré-determinado;</p><br/><br/><ul class="list"><li>O serviço é feito somente no endereço de entrega do produto. Caso o endereço de entrega seja diferente do endereço de cobrança, você deverá fazer o cadastro do mesmo durante a realização do pedido;</li><li style="background:#f0f0f0;margin-bottom:5px;">Para saber se a montagem deste produto pode ser incluída na sua compra, digite o CEP de entrega no campo "Verifique a disponibilidade do frete e montagem", na página deste produto;</li><li>O serviço de montagem é realizada no horário de 8h ás 20h. Não efetuamos montagem aos domingos e feriados;</li><li style="background:#f0f0f0;margin-bottom:5px;">Para o sucesso do seu atendimento, é imprescindível que as informações de endereço, e-mail e telefones do seu cadastro estejam atualizadas;</li><li>É necessário que haja na residência uma pessoa maior de 18 anos para acompanhar o serviço de montagem;</li><li style="background:#f0f0f0;margin-bottom:5px;">A montagem é agendada automaticamente pelo nosso SAC logo após a entrega do produto;</li><li>Para evitar riscos ou danos ao produto, sugerimos que os produtos que necessitam montagem sejam desembalados apenas pelo montador;</li><li style="background:#f0f0f0;margin-bottom:5px;">As instalações de Home Theater, Painéis e Nichos só poderão ser realizadas em paredes de alvenaria;</li><li>Não fazemos instalação de suportes para TV"s no Painel / Home Theater.<br />Problemas causados por montagem própria ou relacionados a adaptações e/ou alterações realizadas no produto não estão cobertos pela Garantia;</li><li style="background:#f0f0f0;margin-bottom:5px;">O prazo máximo para a montagem ser realizada é de até 15 dias úteis depois do recebimento do produto, e nos comprometemos a realizar o agendamento do serviço dentro deste prazo.</li></ul><br/><br/></div>'
        $('.modalMostruario h3').html("Serviço de Montagem de Móveis");
        $('.modalMostruario .modalWrap').html(html);
    },

    ajudaCaracteristica: function() {
        //$('<span class="ajudaCaracteristica">?</>').appendTo('.spec #caracteristicas .value-field');

        $('.search-single-navigator a').each(function() {
            var atributeText = $(this).attr('title');
            var filterText = $(this).text();
            filterText = filterText.replace(atributeText, '<span>' + atributeText + '</span>');
            $(this).text(filterText);
        });

        var caracteristicas = $(".spec #caracteristicas .value-field").text();
        /*caracteristicas = caracteristicas.replace(/,/g, "<br/>");

        vtexjs.catalog.getCurrentProductWithVariations().done(function(product){
            //console.log(product);
        });



        var propName = 1000005;






        var needle = dataLayer[0].productDepartmentId;

        var categoriaId = dataLayer[0].productCategoryId;

        //console.log(needle);



        $(".spec #caracteristicas .value-field").html(caracteristicas);*/

        var categoryId = dataLayer[0].productCategoryId;

        if (!$('.productRelated .prateleira')[0]) {
            $('.productRelated').hide();
        }


        //console.log(categoryId);
        if (categoryId == 1000014 || categoryId == 1000015 || categoryId == 1000016 || categoryId == 1000017 || categoryId == 1000018 || categoryId == 1000019) {

            var cognJson2 = [{
                "agrupamentoName": "Macio ou Firme?",
                "agrupamentoClass": "Macio-ou-Firme-",
                "cognitivos": [{
                    "cognitivosTitle": "Para os idosos",
                    "cognitivosTexto": "Sofás com espumas de Alta Densidade oferecem maior resistência, estabilidade e sustentam melhor o peso, exigindo menor esforço ao sentar e ao levantar, e mantendo a postura adequada."
                }, {
                    "cognitivosTitle": "Superconforto",
                    "cognitivosTexto": "Os sofás com assentos em molas são ainda mais macios e confortáveis."
                }, {
                    "cognitivosTitle": "Encostou, cochilou",
                    "cognitivosTexto": "Tire aquela soneca gostosa recostando a sua cabeça no braço acochoado do sofá."
                }]
            }, {
                "agrupamentoName": "Tamanho",
                "agrupamentoClass": "Tamanho",
                "cognitivos": [{
                        "cognitivosTitle": "Para os mais baixos",
                        "cognitivosTexto": "O assento com menor profundidade permite que pessoas de baixa estatura se sentem confortavelmente encostando os pés no chão e apoiando devidamente as costas no encosto do sofá."
                    }, {
                        "cognitivosTitle": "Para os mais altos",
                        "cognitivosTexto": "O assento em maior profundidade permite que pessoas de alta estatura se sentem confortavelmente, sem que as pernas fiquem 'sobrando', o que prejudica a postura."
                    }, {
                        "cognitivosTitle": "Para ambientes pequenos",
                        "cognitivosTexto": "Salas pequenas pedem Sofás na medida certa. Neste caso, priorize modelos de 2 lugares e de larguras menores."
                    },

                    {
                        "cognitivosTitle": "Cabe todo mundo",
                        "cognitivosTexto": "Ideais para famílias grandes ou para quem recebe muita gente em casa, sofás com largura acima deÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ são perfeitos."
                    }
                ]
            }, {
                "agrupamentoName": "Com o Tecido Certo",
                "agrupamentoClass": "Com-o-tecido-certo",
                "cognitivos": [{
                    "cognitivosTitle": "Fácil de limpar",
                    "cognitivosTexto": "O principal diferencial do Sofá com tecido sintético é a praticidade na limpeza: basta usar um pano úmido ou esponja com detergente ou sabão de coco, e a sujeira vai embora."
                }, {
                    "cognitivosTitle": "Xô Alergia!",
                    "cognitivosTexto": "Ao limpar com frequência o sofá de tecido sintético, você diminui o acúmulo de poeira e previne alergias respiratórias."
                }, {
                    "cognitivosTitle": "Para as crianças",
                    "cognitivosTexto": "Como podem ser limpos, os sofás com tecidos sintéticos funcionam bem para quem tem crianças pequenas em casa."
                }, {
                    "cognitivosTitle": "Para casa de praia",
                    "cognitivosTexto": "O uso de roupas de banho molhadas no sofá exige menor absorção da água pelo tecido e facilidade na higienização, características do formato sintético."
                }, {
                    "cognitivosTitle": "Calor nem pensar",
                    "cognitivosTexto": "Os sofás com tecidos naturais respiram, por isso são mais frescos e mantêm temperatura constante, seja no inverno ou no verão."
                }, {
                    "cognitivosTitle": "Toque mais macio",
                    "cognitivosTexto": "Os sofás com tecidos naturais são mais macios e por isso oferecem mais conforto."
                }]
            }, {
                "agrupamentoName": "Design",
                "agrupamentoClass": "Design",
                "cognitivos": [{
                    "cognitivosTitle": "Cinema em Casa",
                    "cognitivosTexto": "Ver um filminho demanda conforto, de pernas para o ar. Sofás Retráteis funcionam de forma excelente para esticar o esqueleto."
                }, {
                    "cognitivosTitle": "Para Relaxar",
                    "cognitivosTexto": "Os modelos Reclináveis lhe permitem reclinar o encosto para trás, oferecendo ainda mais relaxamento e conforto."
                }]
            }];

        }

        if (categoryId == 1000021) {

            var cognJson2 = [{
                "agrupamentoName": "Tamanho",
                "agrupamentoClass": "Tamanho",
                "cognitivos": [{
                    "cognitivosTitle": "Para ambientes pequenos",
                    "cognitivosTexto": "Sofás-cama com até 1,8m atendem muito bem a cômodos com pouco espaço."
                }]
            }, {
                "agrupamentoName": "Praticidade",
                "agrupamentoClass": "Praticidade",
                "cognitivos": [{
                    "cognitivosTitle": "Superprático",
                    "cognitivosTexto": "As alças para puxar facilitam a conversão do sofá em cama com apenas um movimento, sem complicação!"
                }]
            }, {
                "agrupamentoName": "Com o Tecido Certo",
                "agrupamentoClass": "Com-o-tecido-certo",
                "cognitivos": [{
                    "cognitivosTitle": "Fácil de Limpar",
                    "cognitivosTexto": "O principal diferencial do Sofá-Cama com tecido sintético é a praticidade na limpeza: basta usar um pano úmido ou esponja com detergente ou sabão de coco, e a sujeira vai embora."
                }, {
                    "cognitivosTitle": "Xô, alergia!",
                    "cognitivosTexto": "Ao limpar com frequência o sofá-cama de tecido sintético, você diminui o acúmulo de poeira e previne alergias respiratórias."
                }, {
                    "cognitivosTitle": "Para as crianças",
                    "cognitivosTexto": "Como podem ser limpos, os sofás-cama com tecidos sintéticos funcionam bem para quem tem crianças pequenas em casa."
                }, {
                    "cognitivosTitle": "Para a Casa de Praia",
                    "cognitivosTexto": "O uso de roupas de banho molhadas no sofá-cama exige menor absorção da água pelo tecido e facilidade na higienização, características do formato sintético."
                }, {
                    "cognitivosTitle": "Calor nem pensar",
                    "cognitivosTexto": "Os sofás-cama com tecidos naturais respiram, por isso são mais frescos e mantêm temperatura constante, seja no inverno ou no verão."
                }, {
                    "cognitivosTitle": "Toque Mais Macio",
                    "cognitivosTexto": "Os sofás-cama com tecidos naturais são mais macios e por isso oferecem mais conforto."
                }]
            }];
        }

        if (categoryId == 1000024) {

            var cognJson2 = [

                {
                    "agrupamentoName": "Para quê?",
                    "agrupamentoClass": "Para-que-",
                    "cognitivos": [{
                        "cognitivosTitle": "Sua leitura em dia",
                        "cognitivosTexto": "Utilize sua Poltrona Decorativa com Estofado no braço para ler o seu livro ou jornal de um jeito bem confortável!"
                    }]
                }
            ];
        }

        if (categoryId == 1000025) {

            var cognJson2 = [

                {
                    "agrupamentoName": "Para quê?",
                    "agrupamentoClass": "Para-que-",
                    "cognitivos": [{
                        "cognitivosTitle": "Para assistir TV",
                        "cognitivosTexto": "Poltronas com sistema TV lhe permitem assistir TV com o máximo conforto, reclinando-se para trás e de pernas para o ar."
                    }, {
                        "cognitivosTitle": "Sua leitura em dia",
                        "cognitivosTexto": "Os braços acolchoados das Poltronas com sistema TV favorecem o seu uso para os momentos de leitura."
                    }]
                }, {
                    "agrupamentoName": "Praticidade",
                    "agrupamentoClass": "Praticidade",
                    "cognitivos": [{
                        "cognitivosTitle": "Fácil de reclinar",
                        "cognitivosTexto": "Os modelos que exigem menos esforço e complexidade para reclinar são ideais para idosos ou pessoas com força limitada."
                    }]
                }
            ];




        }

        if (categoryId == 1000029 || categoryId == 1000030 || categoryId == 1000031) {

            var cognJson2 = [{
                "agrupamentoName": "Tampo",
                "agrupamentoClass": "Tampo",
                "cognitivos": [{
                    "cognitivosTitle": "Superfície que não Mancha",
                    "cognitivosTexto": "Esqueça a preocupação com manchas de copo na sua mesa: com o tampo de vidro, isso não acontece mais!"
                }]
            }];


        }



        if (categoryId == 1000023 || categoryId == 1000026 || categoryId == 1000027) {

            //console.log('existe');

            var cognJson2 = [{
                "agrupamentoName": "Use também para",
                "agrupamentoClass": "Use-tambem-para",
                "cognitivos": [{
                    "cognitivosTitle": "Guardar seus objetos",
                    "cognitivosTexto": "Modelos com portas ou gavetas são ideais para quem deseja reservar um espaço da estante para guardar pequenos objetos."
                }, {
                    "cognitivosTitle": "Esconder a bagunça",
                    "cognitivosTexto": "Se você não consegue evitar a bagunça, deixe-a escondida atrás das portas da estante!"
                }]
            }, {
                "agrupamentoName": "Tamanho",
                "agrupamentoClass": "Tamanho",
                "cognitivos": [{
                    "cognitivosTitle": "Para Sala Estreita",
                    "cognitivosTexto": "Racks com pouca profundidade (até 45cm) não deixam você perder grande espaço para a circulação na sua Sala de Estar."
                }, {
                    "cognitivosTitle": "Largura ajustável",
                    "cognitivosTexto": "O Rack Extensível proporciona o aproveitamento máximo da parede de sua Sala e ainda garante a versatilidade na sua forma de uso."
                }, {
                    "cognitivosTitle": "Para Tvs grandes",
                    "cognitivosTexto": "Para colocar as maiores Tvs, escolha Painéis com estrutura para TV a partir de 48."
                }, {
                    "cognitivosTitle": "Para Tvs pequenas",
                    "cognitivosTexto": "Para colocar uma Tv pequena, escolha Painéis com estrutura para TV até 32."
                }, {
                    "cognitivosTitle": "Para Tvs médias",
                    "cognitivosTexto": "Para colocar uma TV média, escolha Painéis com estrutura para TV a partir de 34 até 46."
                }]
            }];


        }

        if (categoryId == 1000028) {

            var cognJson2 = [{
                "agrupamentoName": "Use também para",
                "agrupamentoClass": "Use-tambem-para",
                "cognitivos": [{
                    "cognitivosTitle": "Para decorar melhor",
                    "cognitivosTexto": "Os pontos de luz no Painel embelezam e dão um toque decorativo a mais na sua sala de estar."
                }]
            }, {
                "agrupamentoName": "Tamanho",
                "agrupamentoClass": "Tamanho",
                "cognitivos": [{
                    "cognitivosTitle": "Para Sala Estreita",
                    "cognitivosTexto": "Racks com pouca profundidade (até 45cm) não deixam você perder grande espaço para a circulação na sua Sala de Estar."
                }, {
                    "cognitivosTitle": "Largura ajustável",
                    "cognitivosTexto": "O Rack Extensível proporciona o aproveitamento máximo da parede de sua Sala e ainda garante a versatilidade na sua forma de uso."
                }, {
                    "cognitivosTitle": "Para Tvs grandes",
                    "cognitivosTexto": "Para colocar as maiores Tvs, escolha Painéis com estrutura para TV a partir de 48."
                }, {
                    "cognitivosTitle": "Para Tvs pequenas",
                    "cognitivosTexto": "Para colocar uma Tv pequena, escolha Painéis com estrutura para TV até 32."
                }, {
                    "cognitivosTitle": "Para Tvs médias",
                    "cognitivosTexto": "Para colocar uma TV média, escolha Painéis com estrutura para TV a partir de 34 até 46."
                }]
            }];
        }

        if (categoryId == 1000112) {

            var cognJson2 = [{
                "agrupamentoName": "Onde usar?",
                "agrupamentoClass": "Onde usar-",
                "cognitivos": [{
                    "cognitivosTitle": "Para a Sala de Jantar",
                    "cognitivosTexto": "Os Tapetes com Pelo curto funcionam melhor em salas de Jantar pois facilitam a movimentação das cadeiras, além de serem mais fáceis de limpar."
                }, {
                    "cognitivosTitle": "Para a Sala de Estar",
                    "cognitivosTexto": "Os Tapetes de Pelo longo são melhores nas Salas de Estar, pois dão um toque final de conforto e beleza e são a cereja do bolo da sala."
                }, {
                    "cognitivosTitle": "Para colocar ao lado da Cama",
                    "cognitivosTexto": "Os Tapetes com até 1,20m de largura são ótimos para colocar do ladinho da cama e aquecer seus pés. "
                }]
            }];


        }

        if (categoryId == 1000056) {

            var cognJson2 = [{
                "agrupamentoName": "Tamanho",
                "agrupamentoClass": "Tamanho",
                "cognitivos": [{
                    "cognitivosTitle": "Cabe tudo!",
                    "cognitivosTexto": "Modelos amplos de Bufê acima de 1,30m de largura tornam possível que você guarde tudo o que precisa dentro deles."
                }, ]
            }, {
                "agrupamentoName": "Tampo",
                "agrupamentoClass": "Tampo",
                "cognitivos": [{
                    "cognitivosTitle": "Superfície que não Mancha",
                    "cognitivosTexto": "Esqueça a preocupação com manchas de copo na sua mesa: com o tampo de vidro, isso não acontece mais!"
                }, ]
            }];


        }

        if (categoryId == 1000057) {

            var cognJson2 = [{
                "agrupamentoName": "Tamanho",
                "agrupamentoClass": "Tamanho",
                "cognitivos": [{
                    "cognitivosTitle": "Bem espaçosa",
                    "cognitivosTexto": "Cadeiras com Assento acima de 0,48m são mais largas e amplas que as comuns."
                }, ]
            }, {
                "agrupamentoName": "Assento",
                "agrupamentoClass": "Assento",
                "cognitivos": [{
                    "cognitivosTitle": "Fácil de Limpar",
                    "cognitivosTexto": "O principal diferencial do revestimento em Tecido Sintético é a praticidade na limpeza: basta usar um pano úmido ou esponja com detergente ou sabão de coco, e a sujeira vai embora."
                }, {
                    "cognitivosTitle": "Para quem tem animais",
                    "cognitivosTexto": "Os sofás de tecidos sintéticos são mais resistentes e limpáveis com um pano ou esponja, o que ajuda na eliminação dos pêlos do seu animal."
                }, {
                    "cognitivosTitle": "Para quem tem crianças",
                    "cognitivosTexto": "Como podem ser limpos, os revestimentos em tecidos sintéticos funcionam bem para quem tem crianças pequenas em casa."
                }, {
                    "cognitivosTitle": "Para a Casa de Praia",
                    "cognitivosTexto": "Sentar nas cadeiras com roupas de banho molhadas exige menor absorção da água pelo tecido e facilidade na higienização, características do revestimento em Tecido Sintético."
                }, {
                    "cognitivosTitle": "Feito para Lavar",
                    "cognitivosTexto": "Com os modelos de Plástico, você pode dar, literalmente, um banho de limpeza: lave a cadeira e tenha-a novinha em folha!"
                }, {
                    "cognitivosTitle": "Toque Mais Macio",
                    "cognitivosTexto": "Os revestimentos em tecidos naturais são mais macios e por isso oferecem mais conforto do que os tecidos sintéticos."
                }, {
                    "cognitivosTitle": "Para quem transpira muito",
                    "cognitivosTexto": "Os revestimentos em tecidos naturais possuem melhor respiro e absorvem mais a umidade que os tecidos sintéticos."
                }]
            }, {
                "agrupamentoName": "Encosto",
                "agrupamentoClass": "Encosto",
                "cognitivos": [{
                    "cognitivosTitle": "Suja menos",
                    "cognitivosTexto": "O encosto com partes em madeira ou cromado além do revestimento em tecido evita sujá-lo ao manusear com as mãos."
                }, ]
            }];


        }

        if (categoryId == 1000058) {

            var cognJson2 = [{
                "agrupamentoName": "Tampo",
                "agrupamentoClass": "Tampo",
                "cognitivos": [{
                    "cognitivosTitle": "Superfície que não Mancha",
                    "cognitivosTexto": "Esqueça a preocupação com manchas de copo na sua mesa: com o tampo de vidro, isso não acontece mais!"
                }, ]
            }, {
                "agrupamentoName": "Para quê?",
                "agrupamentoClass": "Para que-",
                "cognitivos": [{
                    "cognitivosTitle": "RefeiçÃµes em família",
                    "cognitivosTexto": "Mesas a partir de 6 lugares acomodam muito bem uma família inteira."
                }]
            }];


        }

        if (categoryId == 1000040) {

            var cognJson2 = [{
                "agrupamentoName": "Somos um casal",
                "agrupamentoClass": "Somos-um-casal",
                "cognitivos": [{
                    "cognitivosTitle": "Com pesos muito diferentes",
                    "cognitivosTexto": "Os ColchÃµes de Mola se adaptam a todos os biotipos e não sofrem desgaste desigual decorrente da diferença de peso."
                }, {
                    "cognitivosTitle": "Que se mexe muito",
                    "cognitivosTexto": "Os colchÃµes com Molas Pocket não transmitem o movimento para as outras molas: ele fica restrito ao ponto de contato do corpo. Desse modo, você pode se mexer Ã   vontade que o outro lado fica imóvel."
                }]
            }, {
                "agrupamentoName": "Macio ou firme?",
                "agrupamentoClass": "Macio-ou-firme-",
                "cognitivos": [{
                        "cognitivosTitle": "Maciez no molejo",
                        "cognitivosTexto": "O colchÃµes com Molas Bonnel são excepecionalmente macios."
                    }, {
                        "cognitivosTitle": "Espuma Macia",
                        "cognitivosTexto": "ColchÃµes com Espumas de densidades menores oferecem menor rigidez e são mais macias. São ideais para pessoas mais leves."
                    }, {
                        "cognitivosTitle": "Maior firmeza",
                        "cognitivosTexto": "Os colchÃµes de Espumas de maior densidade são mais resistentes e rígidas, ideais para pessoas mais pesadas."
                    }

                ]
            }, {
                "agrupamentoName": "Com o Tecido Certo",
                "agrupamentoClass": "Com-o-tecido-certo",
                "cognitivos": [{
                        "cognitivosTitle": "Para quem transpira muito",
                        "cognitivosTexto": "Os tecidos naturais possuem melhor respiro e absorvem mais a umidade que os tecidos sintéticos."
                    }

                ]
            }, {
                "agrupamentoName": "Utilidades",
                "agrupamentoClass": "Utilidades",
                "cognitivos": [{
                    "cognitivosTitle": "Para sentar na beira da cama",
                    "cognitivosTexto": "Com a barragem de Poliframe nas espumas, calçar um sapato sentado Ã   beira da cama deixa de ser penoso e você não desliza para fora do colchão."
                }, {
                    "cognitivosTitle": "Com alças",
                    "cognitivosTexto": "As alças de um colchão servem para facilitar o seu giro, movimento importante para manter a sua durabilidade."
                }]
            }];


        }

        if (categoryId == 1000034 || categoryId == 1000035 || categoryId == 1000036) {

            var cognJson2 = [{
                "agrupamentoName": "Tamanho",
                "agrupamentoClass": "Tamanho",
                "cognitivos": [{
                    "cognitivosTitle": "Cabe tudo!",
                    "cognitivosTexto": "Modelos amplos de armário (acima de 2,4m de largura) tornam possível que você guarde tudo o que precisa dentro deles."
                }, {
                    "cognitivosTitle": "Para ambientes pequenos",
                    "cognitivosTexto": "Estes Armários se encaixam bem em paredes de até 2m de largura e os tornam perfeitos para ambientes pequenos."
                }, {
                    "cognitivosTitle": "Espaço bem-dividido para 2",
                    "cognitivosTexto": "Poupe-se de desentendimentos com Armários de divisão igual para 2 pessoas."
                }]
            }, {
                "agrupamentoName": "Utilidades",
                "agrupamentoClass": "Utilidades",
                "cognitivos": [{
                    "cognitivosTitle": "Com espaço para Ternos",
                    "cognitivosTexto": "Com profundidade acima de 0,58m, é possível acomodar seus ternos corretamente sem amassá-los."
                }, {
                    "cognitivosTitle": "Guarde com Segurança",
                    "cognitivosTexto": "Armários com Chave trazem mais segurança na hora de guardar seus pertences."
                }, {
                    "cognitivosTitle": "Para melhorar a Organização",
                    "cognitivosTexto": "As Colmeias são excelentes para facilitar a organização dos seus itens."
                }, {
                    "cognitivosTitle": "Muitas, muitas Gavetas!",
                    "cognitivosTexto": "Espaço é o que não vai faltar. Muitas gavetas no Armário fazem toda a diferença na organização dos seus pertences!"
                }, {
                    "cognitivosTitle": "Com espaço para Vestidos",
                    "cognitivosTexto": "O Espaço para Vestidos é importante para que você possa visualizá-los, sem amassar na barra!"
                }, {
                    "cognitivosTitle": "Gavetas Amigáveis",
                    "cognitivosTexto": "As Gavetas com corrediça telescópica suportam muito peso e permitem a visualização total do conteúdo da gaveta ao abri-la."
                }]
            }];


        }



        if (categoryId == 1000037) {

            var cognJson2 = [{
                "agrupamentoName": "Utilidades",
                "agrupamentoClass": "Utilidades",
                "cognitivos": [{
                    "cognitivosTitle": "Bom para combinar",
                    "cognitivosTexto": "A ausência de puxadores elimina a necessidade de combinar com os puxadores dos outros móveis do cômodo"
                }, {
                    "cognitivosTitle": "Muitas, muitas Gavetas!",
                    "cognitivosTexto": "Muitas gavetas fazem toda a diferença na organização dos seus pertences. Espaço é o que não vai faltar."
                }, {
                    "cognitivosTitle": "Gavetas Amigáveis",
                    "cognitivosTexto": "As Gavetas com corrediça telescópica são mais silenciosas, suportam melhor o peso e permitem a visualização total do conteúdo da gaveta ao abri-la."
                }, {
                    "cognitivosTitle": "Guarde com Segurança",
                    "cognitivosTexto": "Cômodas com Chave trazem mais segurança na hora de guardar seus pertences."
                }, {
                    "cognitivosTitle": "Com espaço para os sapatos",
                    "cognitivosTexto": "Com espaço exclusivo para guardar seus sapatos, seus calçados também não ficam de fora da organização."
                }]
            }, {
                "agrupamentoName": "Tamanho",
                "agrupamentoClass": "Tamanho",
                "cognitivos": [{
                    "cognitivosTitle": "Para ambientes pequenos",
                    "cognitivosTexto": "Cômodas de até 90cm de largura se encaixam muito bem em qualquer cantinho e por isso são ideais para espaços pequenos."
                }]
            }];


        }

        if (categoryId == 1000038) {

            var cognJson2 = [{
                "agrupamentoName": "Tampo",
                "agrupamentoClass": "Tampo",
                "cognitivos": [{
                    "cognitivosTitle": "Superfície que não Mancha",
                    "cognitivosTexto": "Esqueça a preocupação com manchas de copo na sua mesa: com o tampo de vidro, isso não acontece mais!"
                }, ]
            }, {
                "agrupamentoName": "Utilidades",
                "agrupamentoClass": "Utilidades",
                "cognitivos": [{
                    "cognitivosTitle": "Bom para combinar",
                    "cognitivosTexto": "A ausência de puxadores elimina a necessidade de combinar com os puxadoes dos outros móveis do cômodo"
                }]
            }];


        }

        if (categoryId == 1000064) {

            var cognJson2 = [{
                "agrupamentoName": "Tampo",
                "agrupamentoClass": "Tampo",
                "cognitivos": [{
                    "cognitivosTitle": "Superfície que não Mancha",
                    "cognitivosTexto": "Esqueça a preocupação com manchas de copo na sua mesa: com o tampo de vidro, isso não acontece mais!"
                }, ]
            }, {
                "agrupamentoName": "Utilidades",
                "agrupamentoClass": "Utilidades",
                "cognitivos": [{
                    "cognitivosTitle": "Bom para combinar",
                    "cognitivosTexto": "A ausência de puxadores elimina a necessidade de combinar com os puxadoes dos outros móveis do cômodo"
                }]
            }];


        }

        if (categoryId == 1000078) {



            var cognJson2 = [{
                "agrupamentoName": "Para quem?",
                "agrupamentoClass": "Para quem-",
                "cognitivos": [{
                    "cognitivosTitle": "Para os meninos",
                    "cognitivosTexto": "Móveis que tenham a cor Azul caem muito bem para o quarto dos meninos."
                }, {
                    "cognitivosTitle": "Para as meninas",
                    "cognitivosTexto": "A cor Rosa é a preferida das meninas. Elas também adoram os móveis do quarto nesta cor!"
                }, {
                    "cognitivosTitle": "Para meninos e meninas",
                    "cognitivosTexto": "Os móveis na cor branca funcionam bem tanto para quarto de meninos quanto de meninas."
                }, {
                    "cognitivosTitle": "Para irmãos",
                    "cognitivosTexto": "Armários com largura a partir de 1,30m atendem bem a 2 crianças."
                }]
            }];




        }

        if (categoryId == 1000079) {

            var cognJson2 = [{
                "agrupamentoName": "Para quê?",
                "agrupamentoClass": "Para-que-",
                "cognitivos": [{
                    "cognitivosTitle": "Facilitar para a mamãe",
                    "cognitivosTexto": "A grade móvel facilita a vida da mamãe na hora de tirar o bebê de dentro do berço, principalmente quando ele está mais sonolento."
                }, {
                    "cognitivosTitle": "Durar até os 4 anos",
                    "cognitivosTexto": "Os berços que viram minicama têm uma vida útil maior para o seu filho, e pode ser usada não só quando ele for bebê."
                }, {
                    "cognitivosTitle": "Ninar o bebê",
                    "cognitivosTexto": "Os pés com balanço permite Ã  mãe ninar o bebê no próprio bercinho."
                }]
            }];
        }


        if (categoryId == 1000080) {

            var cognJson2 = [{
                "agrupamentoName": "Para quem?",
                "agrupamentoClass": "Para-quem-",
                "cognitivos": [{
                    "cognitivosTitle": "2 irmãos e pouco espaço",
                    "cognitivosTexto": "A cama beliche é uma excelente solução para quem tem 2 crianças mas conta com pouco espaço."
                }, {
                    "cognitivosTitle": "Até 3 irmãos e pouco espaço",
                    "cognitivosTexto": "A treliche é uma excelente solução para quem tem até 3 crianças mas conta com pouco espaço. Uma cama no alto, e uma bicama embaixo e todo mundo dorme bem confortável."
                }, {
                    "cognitivosTitle": "Para receber os primos",
                    "cognitivosTexto": "A bicama é uma excelente opção para pequenos hóspedes."
                }]
            }];
        }

        if (categoryId == 1000081) {

            var cognJson2 = [{
                "agrupamentoName": "O que tem?",
                "agrupamentoClass": "O-que-tem-",
                "cognitivos": [{
                    "cognitivosTitle": "Com lixeirinha fácil",
                    "cognitivosTexto": "Ao trocar a fralda do neném, uma lixeirinha por perto faz toda a diferença."
                }, {
                    "cognitivosTitle": "Gavetas Amigáveis",
                    "cognitivosTexto": "As Gavetas com corrediça telescópica suportam muito peso e permitem a visualização total do conteúdo da gaveta ao abri-la."
                }, {
                    "cognitivosTitle": "Elementos para as meninas",
                    "cognitivosTexto": "A cor Rosa é a preferida das meninas. Elas também adoram os móveis do quarto nesta cor!"
                }]
            }, {
                "agrupamentoName": "Tamanho",
                "agrupamentoClass": "Tamanho",
                "cognitivos": [{
                    "cognitivosTitle": "Para ambientes pequenos",
                    "cognitivosTexto": "Cômodas de até 90cm de largura se encaixam muito bem em qualquer cantinho e por isso são ideais para espaços pequenos."
                }]
            }];
        }

        if (categoryId == 1000082) {

            var cognJson2 = [{
                "agrupamentoName": "Tampo",
                "agrupamentoClass": "Tampo",
                "cognitivos": [{
                    "cognitivosTitle": "Superfície que não Mancha",
                    "cognitivosTexto": "Esqueça a preocupação com manchas de copo na sua mesa: com o tampo de vidro, isso não acontece mais!"
                }]
            }, {
                "agrupamentoName": "Utilidades",
                "agrupamentoClass": "Utilidades",
                "cognitivos": [{
                    "cognitivosTitle": "Bom para combinar",
                    "cognitivosTexto": "A ausência de puxadores elimina a necessidade de combinar com os puxadoes dos outros móveis do cômodo."
                }]
            }];
        }

        if (categoryId == 1000073 || categoryId == 1000074 || categoryId == 1000075 || categoryId == 1000076) {

            var cognJson2 = [{
                "agrupamentoName": "Para quê?",
                "agrupamentoClass": "Para-que-",
                "cognitivos": [{
                    "cognitivosTitle": "Para mover com facilidade",
                    "cognitivosTexto": "Mesas em plástico são leves, facilitando o carregamento de um lado para o outro no jardim, e são fáceis de limpar."
                }, {
                    "cognitivosTitle": "Para combinar fácil",
                    "cognitivosTexto": "Mesas em madeira são muito versáteis na hora de combinar."
                }, {
                    "cognitivosTitle": "Para maior resistência",
                    "cognitivosTexto": "Mesas em alumínio são bastante resistentes, até mesmo quando expostas ás açÃµes da natureza."
                }]
            }];
        }

        if (cognJson2) {

            //console.log(cognJson2);

            for (var i = 0; i < cognJson2.length; i++) {

                var cognClass = cognJson2[i].agrupamentoClass;
                //console.log(cognJson2[i]);

                if ($(".spec #caracteristicas .value-field." + cognJson2[i].agrupamentoClass)[0]) {
                    var listCogn = $(".spec #caracteristicas .value-field." + cognJson2[i].agrupamentoClass).text();
                    var congHtml = '<ul class="listCognitivos"/>';
                    var cognList = $(".spec #caracteristicas .value-field." + cognJson2[i].agrupamentoClass);

                    $(cognList).html(congHtml);
                    //$(".spec #caracteristicas .value-field."+cognJson2[i].agrupamentoClass).html(congHtml);



                    for (var y = 0; y < cognJson2[i].cognitivos.length; y++) {
                        ////console.log(cognJson2[i].cognitivos[y]);

                        ////console.log(listCogn);

                        var searchText = listCogn.indexOf(cognJson2[i].cognitivos[y].cognitivosTitle);

                        if (searchText != -1) {
                            //console.log(cognJson2[i].cognitivos[y].cognitivosTitle);
                            //console.log(cognJson2[i].cognitivos[y].cognitivosTexto);

                            var checkClass = '.' + cognJson2[i].agrupamentoClass + ' .listCognitivos';
                            var cognitivosTitle = cognJson2[i].cognitivos[y].cognitivosTitle;
                            var cognitivosText = cognJson2[i].cognitivos[y].cognitivosTexto;
                            //$('<span>teste</span>')appendTo('.listCognitivos');

                            var cognInner = '<li>' + cognitivosTitle + ' <div class="ajudaCaracteristica"><span class="icon-help">?</span> <span class="cognitivosText">' + cognitivosText + '</span></div></li>'

                            $(cognInner).appendTo($(checkClass));

                            //console.log(checkClass);


                        }



                    }
                }
            }
        }
    },

    buyTogether: function() {

        //console.log("funciona");

        if ($("#divCompreJunto table")[0]) {
            if (!$(".buyContent")[0]) {
                var test = $("#divCompreJunto .buy").html();
                test = test.replace("Valor total:", "");
                test = test.replace("Comprando junto", "");
                var buyButton = $(".comprar-junto").html();


                if (test.match("<br>(.*)<strong>")) {
                    var testRE = test.match("<br>(.*)<strong>");
                    var testREA = test.match("você economiza(.*)</strong>");
                    var economic = testREA[0].replace("você economiza:", "<span>você economiza</span>");

                    var buyContent = '<div class="buyContent">\
                                        <h5><i>2</i> <span>Comprando Junto</span></h5>\
                                        <div class="inner">\
                                        <span class="price">' + testRE[1] + '</span>\
                                        <span class="economiza">' + economic + '</span>\
                                        <div class="buyButton">' + buyButton + '</div></div>\
                                        </div>';
                } else {
                    //var economic = "";

                    test = test.replace("Por apenas", "");
                    test = test.replace("12x", "");
                    test = test.replace("de", "");



                    var buyContent = '<div class="buyContent simple">\
                                        <h5><i>2</i> <span>Comprando Junto</span></h5>\
                                        <div class="inner">\
                                        <span class="price">' + test + '</span>\
                                        </div>';


                }
                ////console.log(testRE[1]);
                ////console.log(testREA);
                $("#divCompreJunto .buy").html(buyContent);
            }
        }


    },

    montagem: function() {
        /* $('input.freight-btn').val("CALCULAR FRETE");

        $('input.freight-btn').click(function() {
            var cepUser = parseInt($('.freight-zip-box').val().replace('-', ''));
            var clientMonta = $('.productDescription td.Cliente-Monta').text();
            var montagem = $('.productDescription td.Montagem').text();
            var ventilador = $('.productDescription td.ventilador').text();

            var fnsMontagem = function(val) {
                $('.box.frete .montagem').fadeIn();
                    //mensagem para os CEPs de ilhas
                if (val == 81 || val == 82 || val == 89) {
                        $('.montagem').html("");
                        $('.montagem').html("<span style='font-size:14px;font-weight:bold;color:#ffcc32;'>A Toque a Campainha não faz entregas em CEPs que necessitem de transporte fluvial / marítimo. <br/><span style='font-weight:normal;color:#fff;'>Para mais informaçÃµes, ligue para (21) 3296-9090</span>");
                        $(".freight-values").html("<span style='display:none !important;'> </span>");
                        }
                else if (val == 1) {
                    if(ventilador === "1") {
                        $('.box.frete strong').html("Este produto não inclui montagem e instalação.");
                    } else if (clientMonta === "Não" && montagem === "Sim") {
                        $('.box.frete strong').html("Este produto possui montagem gratuita para este CEP.");
                    } else if (clientMonta === "Sim" && montagem === "Sim") {
                        $('.box.frete strong').html("Montagem não disponível para este produto. Produto segue desmontado.");
                    } else if (montagem === "Não") {
                        $('.box.frete strong').html("Este produto seguirá montado.");
                    }

                } else {
                    $('.box.frete strong').html("A Toque a Campainha ainda não faz montagem para este CEP.");
                    //$('.box.frete .montagem').fadeOut();
                };

            }



            $.ajax({
                    url: "http://webapi.toqueacampainha.com.br/api/CEP/ConsultaCEP?cep=" + cepUser,
                    dataType: 'json'
                })
                .done(function(data) {
                    //console.log(data.montagem)
                    fnsMontagem(data.VerificaMontagem);

                })
                .fail(function() {
                    $.ajax({
                            url: "http://cepapi.toqueacampainha.com.br/api/CEP/ConsultaCEP?cep=" + cepUser,
                            dataType: 'jsonp'
                        })
                        .done(function(data) {
                            fnsMontagem(data.montagem);
                        })
                        .fail(function() {
                            //console.log("ocorreu um erro.");
                        });
                });*/


        /*
                    if (clientMonta === "Não" && montagem === "Sim") {
                        $('.box.frete strong').html("Este produto possui montagem gratuita para este CEP.");
                    } else if (clientMonta === "Sim" && montagem === "Sim") {
                        $('.box.frete strong').html("Montagem não disponível para este produto. Produto segue desmontado.");
                    } else if (montagem === "Não") {
                        $('.box.frete strong').html("Este produto seguirá montado.");
                    }

                    $('.box.frete .montagem').fadeIn();
                } else {
                    if (montagem === "Sim") {
                        $('.box.frete strong').html("CEP fora da área de cobertura para montagem. Produto segue desmontado.");
                    }
                    //$('.box.frete .montagem').fadeOut();
                };
            } else {
                $('.box.frete .montagem').fadeOut();
            }*/


        // $.getScript("http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+cepUser, function(){
        // if(resultadoCEP['uf']!="") {
        // if (resultadoCEP["cidade"] === "Rio%20de%20Janeiro" || resultadoCEP["cidade"] === "Niter%F3i") {
        // if (clientMonta === "Não" && montagem === "Sim") {
        // $('.box.frete strong').html("Este produto possui montagem gratuita para este CEP.");
        // } else if (clientMonta === "Sim" && montagem === "Sim") {
        // $('.box.frete strong').html("Montagem não disponível para este produto. Produto segue desmontado.");
        // } else if (montagem === "Não") {
        // $('.box.frete strong').html("Este produto seguirá montado.");
        // }

        // $('.box.frete .montagem').fadeIn();
        // } else {
        // if (montagem === "Sim") {
        // $('.box.frete strong').html("CEP fora da área de cobertura para montagem. Produto segue desmontado.");
        // }
        // //$('.box.frete .montagem').fadeOut();
        // };
        // } else {
        // $('.box.frete .montagem').fadeOut();
        // }

        // });
        //})
    },

    goToEvaluation: function() {
        var avPosition = $('.preFooter').offset() != undefined ? $('.preFooter').offset().top : 0;
        var avPositionFinal = avPosition - 300 + "px";
        //console.log(avPosition);
        //console.log(avPositionFinal);
        // When #scroll is clicked
        $('#spnRatingProdutoTop').click(function() {
            // Scroll down to 'catTopPosition'
            $('html, body').animate({
                scrollTop: avPositionFinal
            }, 'slow');
            // Stop the link from acting like a normal anchor link
            return false;
        });
    },

    shelfCarousel: function() {
        var shelfContent = ".shelfCarousel";
        var _this = $(this);
        $('<div class="shelfCarousel" />').appendTo('.productRelated .prateleira');

        $(".productRelated .prateleira > ul").each(function() {
            $(this).appendTo(shelfContent);
            //console.log(shelfContent);
        });

        $(shelfContent).slick({
            arrows: true,
            dots: false,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 500
        });
    },

    call: function() {
        if ($("body").hasClass("produto")) {
            // ShippingValue();
            product.boxs();
            product.boleto();
            product.list();
            product.thumbs();
            product.flags();
            product.indisponivel();
            product.acessorios();
            product.modal(); //cria o modal para receber o conteudo

            product.mostruarioTable();
            product.specialSpecs();
            //product.montagem();
            //product.store();
            product.goToEvaluation();
            if ($('body').hasClass('catalogo')) {
                product.ajudaCaracteristica();
            }
            product.shelfCarousel();

        }

    },

    kitAcessorios: function() {
        if ($('body').hasClass('produto-acessorios')) {
            var windowWidth = $(window).width();
            if (windowWidth > 768) {
                $('.cont-detalhes.hide-desk, #buyButtonFloat').remove();
            } else {
                $('.cont-detalhes.hide-mob').remove();
            }

            //FUNÃ‡ÃƒO
            alteraValoresQuatidade = function() {
                    //FORMATANDO O VALOR ORIGINAL DO PRODUTO
                    var total = $(precoOriginal).html().replace('R$ ', '');
                    if (total.indexOf('.') >= 0) {
                        total = total.replace('.', '');
                    }
                    total = total.replace(',', '.');
                    total = total.replace(',', '.');
                    var quantidadeParcela = parseInt($('.descricao-preco .skuBestInstallmentNumber').html());
                    $.each($('.va-compoem-conteudo .box-ambiente'), function(index, val) {
                        //MONTANDO O VALOR TOTAL DO AMBIENTE
                        //CONFORME VAI ALTERANDO A QUANTIDADE DO PRODUTO
                        var valor = parseFloat($(this).find('.ambiente-valoresQuantidade span').html()).toFixed(2);
                        total = (parseFloat(total) + parseFloat(valor)).toFixed(2);
                        $('.va-preco-final').html('<span>Por: R$ ' + total.toString().split(/(?=(?:\d{3})+(?:\.|$))/g).join(".") + '</span>');
                        //MONTANDO O VALOR DA PARCELA CONFORME VAI
                        //VAI ALTERANDO A QUANTIDADE DO PRODUTO
                        var valorParcelado = (parseFloat(total) / parseFloat(quantidadeParcela));
                        $('.va-finalizar .va-valor-parcela-final span').remove();
                        $('.va-finalizar .va-valor-parcela-final').prepend('<span>R$ ' + valorParcelado.toFixed(2) + '</span>');
                        //ADICIONANDO O VALOR NOVO DO PRODUTO NO COMEÃ‡O DA PAGINA
                        $('.descricao-preco .valor-por-v2').remove();
                        $('.descricao-preco .valor-por').before('<em class="valor-por-v2"><span>Por: <strong>R$ ' + total.toString().split(/(?=(?:\d{3})+(?:\.|$))/g).join(".") + '</strong></span></em>')
                            //MONTANDO O NOVO VALOR DO DESCONTO NO BOLETO
                        var totalDesconto = total - (total * 5) / 100;
                        totalDesconto = parseFloat(totalDesconto).toFixed(2)
                        $('#productPrice .boleto p b.boleto-v2').remove();
                        $('#productPrice .boleto p b').before('<b class="boleto-v2">R$ ' + totalDesconto.toString().split(/(?=(?:\d{3})+(?:\.|$))/g).join(".") + '</b>');
                        //ADICIONANDO O VALOR NOVO DA PARCELA NO COMEÃ‡O DA PAGINA
                        $('.descricao-preco .skuBestInstallmentValue-v2').remove();
                        $('.descricao-preco .skuBestInstallmentValue').before('<label class="skuBestInstallmentValue-v2">R$ ' + valorParcelado.toFixed(2) + '</label>');
                        //ESCONDENDO O VALOR DE:, ECONOMIA, POR:, PARCELADO, BOLETO DO PRODUTO FLUTUANTE
                        $('.preco-flutuante .pf-section .pf-de, .preco-flutuante .pf-section .pf-por, .preco-flutuante .pf-section .pf-economia-de, .preco-flutuante .pf-section .pf-ou').addClass('va-hide');
                        //ADICIONANDO O VALOR NOVO DO PRODUTO NO COMEÃ‡O DA PAGINA FLUTUANTE
                        $('.preco-flutuante .pf-section .pf-por-v2').remove();
                        $('.preco-flutuante .pf-section .pf-por').before('<div class="pf-por-v2"><p>Por: R$ ' + total.toString().split(/(?=(?:\d{3})+(?:\.|$))/g).join(".") + '</p></div>')
                            //ADICIONANDO O VALOR NOVO DA PARCELA NO COMEÃ‡O DA PAGINA FLUTUANTE
                        $('.preco-flutuante .pf-section .pf-ou-v2').remove();
                        $('.preco-flutuante .pf-section .pf-ou').before('<div class="pf-ou-v2"><p>ou 10x de R$ ' + valorParcelado.toFixed(2) + ' sem juros <p></div>');
                    });
                }
                //COLOCANDO O CHEKBOX ADICIONAR DENTRO DE CADA LI
            var i = 0;
            $.each($('.prateleira-acessorios fieldset'), function(index, val) {

                var inp = $(this).remove();

                $('.va-add-remover').eq(i).append(inp);
                i++;
            });
            //ACCODION DESCRIÃ‡ÃƒO
            if (windowWidth > 768) {
                $('.va-descricao h3').on('click', function() {
                    //$(this).next().toggle();
                    if ($(this).hasClass('va-ativo')) {
                        $(this).removeClass('va-ativo');
                    } else {
                        $(this).addClass('va-ativo');
                    }
                });
            } else {
                var blueMask = "<div id='moreInfoMask' style='display:none;'><div class='moreInfoContainer'><a id='moreInfoClose'>x</a></div></div>";
                $('body').append(blueMask);
                $('.va-descricao h3').on('click', function() {
                    var moreInfoText = $(this).next();

                    console.log(moreInfoText);

                    $('#moreInfoMask .moreInfoContainer').append(moreInfoText);

                    $('#moreInfoMask').fadeIn('slow');
                });

                $('#moreInfoClose').on('click', function(event) {
                    event.preventDefault();
                    /* Act on the event */
                    $(this).next().remove();

                    $('#moreInfoMask').fadeOut('slow');
                });
            }
            //MONTANDO O VALOR DE DESCONTO NO BOLETO NA VITRINE DO AMBIENTE
            $.each($('.prateleira-acessorios li .va-preco-por'), function(index, val) {
                var vaAvista = $(val).text();
                var vaAvista = vaAvista.replace('R$ ', '').replace('.', '').replace('Por:', '');
                var vaAvista = parseFloat(vaAvista.replace(".", "").replace(",", "."));
                var vaAvista = vaAvista - (vaAvista * 5) / 100;
                $(this).parent().find('.va-avista-final').prepend(parseFloat(vaAvista).toFixed(2));
                //console.log(parseFloat(vaAvista));
            });
            //PEGANDO A IMAGEM PRINCIPAL DO PRODUTO
            var imgPrincipal = $('#image-main');
            $('.va-imagem-destaque').prepend(imgPrincipal);
            var imgAjuste = $('.cont-detalhes .va-imagem-destaque .sku-rich-image-main').attr('src').replace('530-530', '437-277');
            $('.cont-detalhes .va-imagem-destaque .sku-rich-image-main').attr('src', imgAjuste);

            //PEGANDO A QUANTIDADE DE PARCELA DO SISTEMA E COLOCANDO NO AMBIENTE
            var quantidadeParcela = parseInt($('.descricao-preco .skuBestInstallmentNumber').html());
            $('.va-finalizar .va-quantidade').prepend(quantidadeParcela);
            //PEGANDO O VALOR DA PARCELA DO SISTEMA E COLOCANDO NO AMBIENTE
            var valorParcelaInicial = $('.descricao-preco .skuBestInstallmentValue').html();
            $('.va-finalizar .va-valor-parcela-final').prepend('<span>' + valorParcelaInicial + '</span>');

            //PEGANDO O VALOR ORIGINAL DO PRODUTO
            $('.descricao-preco .valor-por').html(' Por: <strong class="skuBestPrice">R$ 0,00</strong>');
            var precoOriginal = $('.descricao-preco .valor-por').html();
            console.log('Esse é o preço inicial ' + precoOriginal);
            $('.va-preco-final').prepend('<span>' + precoOriginal + '</span>');

            //FAZENDO FUNCIONAR + QUANTIDADE
            $.each($('.vaq-conteudo'), function(qpi, qpv) {
                var botaoPlus = $(this).children('.product-quantity-plus');
                var campoQuantidade = $(this).children('.product-quantity-value');
                botaoPlus.click(function(event) {
                    event.preventDefault();
                    campoQuantidade.val(parseInt(campoQuantidade.val()) + 1);
                    //MUDANDO A QUANTIDADE NO BOX AMBIENTE
                    var relPlus = $(this).parents('.va-quantidade').find('.buy-product-checkbox').attr('rel');
                    $.each($('.va-compoem-conteudo').find('[data-rel]'), function() {
                        if ($(this).attr('data-rel') == relPlus) {
                            var qtdCopia = $(this).find('.qtd').html();
                            var qtdCopia = parseFloat(qtdCopia) + 1;
                            $(this).find('.qtd').html(parseFloat(qtdCopia));
                            var valorQuantidadePlus = $(this).find('.ambiente-valores').text();
                            var valorQuantidadePlus = valorQuantidadePlus.replace('R$ ', '').replace('.', '').replace('Por:', '');
                            var valorQuantidadePlus = parseFloat(valorQuantidadePlus.replace(".", "").replace(",", "."));
                            var valorQuantidadePlus = valorQuantidadePlus * qtdCopia;
                            $(this).find('.ambiente-valoresQuantidade span').remove();
                            $(this).find('.ambiente-valoresQuantidade').prepend('<span>' + valorQuantidadePlus + '<span>');
                        }
                    });
                    alteraValoresQuatidade();
                });
            });
            //FAZENDO FUNCIONAR - QUANTIDADE
            $.each($('.vaq-conteudo'), function(qmi, qmv) {
                var botaoMinus = $(this).children('.product-quantity-minus');
                var campoQuantidadeMinus = $(this).children('.product-quantity-value');
                botaoMinus.click(function(event) {
                    event.preventDefault();
                    if (campoQuantidadeMinus.val() <= 1) {
                        return false;
                    } else {
                        campoQuantidadeMinus.val(parseInt(campoQuantidadeMinus.val()) - 1);
                        //MUDANDO A QUANTIDADE NO BOX AMBIENTE
                        var relPlus = $(this).parents('.va-quantidade').find('.buy-product-checkbox').attr('rel');
                        $.each($('.va-compoem-conteudo').find('[data-rel]'), function() {
                            if ($(this).attr('data-rel') == relPlus) {
                                var qtdCopia = $(this).find('.qtd').html();
                                var qtdCopia = parseFloat(qtdCopia) - 1;
                                $(this).find('.qtd').html(parseFloat(qtdCopia));
                                var valorQuantidadeMinus = $(this).find('.ambiente-valores').text();
                                var valorQuantidadeMinus = valorQuantidadeMinus.replace('R$ ', '').replace('.', '').replace('Por:', '');
                                var valorQuantidadeMinus = parseFloat(valorQuantidadeMinus.replace(".", "").replace(",", "."));
                                var valorQuantidadeMinus = valorQuantidadeMinus * qtdCopia;
                                $(this).find('.ambiente-valoresQuantidade span').remove();
                                $(this).find('.ambiente-valoresQuantidade').prepend('<span>' + valorQuantidadeMinus + '<span>');
                            }
                        });
                        alteraValoresQuatidade();

                    }
                });
            });
            //FUNÃ‡ÃƒO
            alteraValoreAdicionar = function() {
                //PEGANDO E FORMATANDO O VALOR ORIGINAL DO PRODUTO
                var total = $(precoOriginal).html().replace('R$ ', '');
                if (total.indexOf('.') >= 0) {
                    total = total.replace('.', '');
                }
                total = total.replace(',', '.');
                total = total.replace(',', '.');

                //total = 0.00;
                //PEGANDO A QUANTIDADE DE PARCELA ORIGINAL DO PRODUTO
                var quantidadeParcela = parseInt($('.descricao-preco .skuBestInstallmentNumber').html());
                //VERIFICANDO OS PRODUTOS ADICIONADO
                $.each($('.va-compoem-conteudo .box-ambiente'), function(index, val) {
                    //MONTANDO O VALOR TOTAL DO AMBIENTE
                    //CONFORME VAI ADICIONANDO O PRODUTO
                    var valor = parseFloat($(this).find('.ambiente-valoresQuantidade span').html()).toFixed(2);
                    total = (parseFloat(total) + parseFloat(valor)).toFixed(2);
                    $('.va-preco-final').html('<span>Por: R$ ' + total.toString().split(/(?=(?:\d{3})+(?:\.|$))/g).join(".") + '</span>');
                    //MONTANDO O VALOR TOTAL DA PARCELA
                    //CONFORME VAI ADICIONANDO O PRODUTO
                    var valorParcelado = (parseFloat(total) / parseFloat(quantidadeParcela));
                    $('.va-finalizar .va-valor-parcela-final span').remove();
                    $('.va-finalizar .va-valor-parcela-final').prepend('<span>R$ ' + valorParcelado.toFixed(2) + '</span>');
                    //ESCONDENDO O VALOR DE:, ECONOMIA, POR:, PARCELADO, BOLETO DO PRODUTO
                    $('.descricao-preco .valor-de, .plugin-preco .economia-de, .descricao-preco .valor-por, .descricao-preco .skuBestInstallmentValue, #productPrice .boleto p b').addClass('va-hide');
                    //ADICIONANDO O VALOR NOVO DO PRODUTO NO COMEÃ‡O DA PAGINA
                    $('.descricao-preco .valor-por-v2').remove();
                    $('.descricao-preco .valor-por').before('<em class="valor-por-v2"><span>Por: <strong>R$ ' + total.toString().split(/(?=(?:\d{3})+(?:\.|$))/g).join(".") + '</strong></span></em>')
                        //MONTANDO O NOVO VALOR DO DESCONTO NO BOLETO
                    var totalDesconto = total - (total * 5) / 100;
                    totalDesconto = parseFloat(totalDesconto).toFixed(2)
                    $('#productPrice .boleto p b.boleto-v2').remove();
                    $('#productPrice .boleto p b').before('<b class="boleto-v2">R$ ' + totalDesconto.toString().split(/(?=(?:\d{3})+(?:\.|$))/g).join(".") + '</b>');
                    //ADICIONANDO O VALOR NOVO DA PARCELA NO COMEÃ‡O DA PAGINA
                    $('.descricao-preco .skuBestInstallmentValue-v2').remove();
                    $('.descricao-preco .skuBestInstallmentValue').before('<label class="skuBestInstallmentValue-v2">R$ ' + valorParcelado.toFixed(2) + '</label>');
                    //ESCONDENDO O VALOR DE:, ECONOMIA, POR:, PARCELADO, BOLETO DO PRODUTO FLUTUANTE
                    $('.preco-flutuante .pf-section .pf-de, .preco-flutuante .pf-section .pf-por, .preco-flutuante .pf-section .pf-economia-de, .preco-flutuante .pf-section .pf-ou').addClass('va-hide');
                    //ADICIONANDO O VALOR NOVO DO PRODUTO NO COMEÃ‡O DA PAGINA FLUTUANTE
                    $('.preco-flutuante .pf-section .pf-por-v2').remove();
                    $('.preco-flutuante .pf-section .pf-por').before('<div class="pf-por-v2"><p>Por: R$ ' + total.toString().split(/(?=(?:\d{3})+(?:\.|$))/g).join(".") + '</p></div>')
                        //ADICIONANDO O VALOR NOVO DA PARCELA NO COMEÃ‡O DA PAGINA FLUTUANTE
                    $('.preco-flutuante .pf-section .pf-ou-v2').remove();
                    $('.preco-flutuante .pf-section .pf-ou').before('<div class="pf-ou-v2"><p>ou 10x de R$ ' + valorParcelado.toFixed(2) + ' sem juros <p></div>');
                });
            };
            //FUNÃ‡ÃƒO QUE FAZ MOSTRAR O VALOR ORIGINAL DO PRODUÃ‡ÃƒO QUANDO NÃƒO ADICIONAR NENHU
            mostraValorOriginal = function() {
                //PEGANDO E FORMATANDO O VALOR ORIGINAL DO PRODUTO
                var total = $(precoOriginal).html().replace('R$ ', '');
                if (total.indexOf('.') >= 0) {
                    total = total.replace('.', '');
                }
                //PEGANDO A QUANTIDADE DE PARCELAS
                var quantidadeParcela = parseInt($('.descricao-preco .skuBestInstallmentNumber').html());
                //PEGANDO O VALOR DA URL ORIGINAL DO PRODUTO
                var urlOriginal = $('#BuyButton a').attr('href');
                //VERIFICANDO SE EXISTE PRODUTO ADICIONADO PARA CALCULAR
                if ($('.va-compoem-conteudo').find('.box-ambiente').length > 0) {
                    alteraValoreAdicionar();
                } else {
                    //COLOCANDO O VALOR ORIGINAL DO PRODUTO QUANDO NÃƒO SELECIONAR NENHUM ACESSORIO
                    total = parseFloat(total).toFixed(2);
                    $('.va-preco-final').html('<span>Por: R$ ' + total.toString().split(/(?=(?:\d{3})+(?:\.|$))/g).join(".") + '</span>');
                    //COLOCANDO O VALOR ORIGINAL DA PARCELA QUANDO NÃƒO SELECIONAR NENHUM ACESSORIO
                    $('.va-finalizar .va-valor-parcela-final span').remove();
                    $('.va-finalizar .va-valor-parcela-final').prepend('<span>' + valorParcelaInicial + '</span>');
                    //MOSTANDO O VALOR DE:, ECONOMIA, POR:, PARCELADO, BOLETO DO PRODUTO  QUANDO NÃƒO SELECIONAR NENHUM ACESSORIO
                    $('.descricao-preco .valor-de, .plugin-preco .economia-de, .descricao-preco .valor-por, .descricao-preco .skuBestInstallmentValue, #productPrice .boleto p b').removeClass('va-hide');
                    //REMOVENDO O VALORES ADICIONADO DO ACESSORIOS
                    $('.descricao-preco .valor-por-v2, #productPrice .boleto p b.boleto-v2, .descricao-preco .skuBestInstallmentValue-v2').remove();
                    //MOSTANDO O VALOR DE:, ECONOMIA, POR:, PARCELADO, BOLETO DO PRODUTO FLUTUANTE
                    $('.preco-flutuante .pf-section .pf-de, .preco-flutuante .pf-section .pf-por, .preco-flutuante .pf-section .pf-economia-de, .preco-flutuante .pf-section .pf-ou').removeClass('va-hide');
                    //REMOVENDO O VALORES ADICIONADO DO ACESSORIOS FLUTUANTE
                    $('.preco-flutuante .pf-section .pf-por-v2, .preco-flutuante .pf-section .pf-ou-v2').remove();
                }
            };
            //CLICANDO NO BOTÃƒO ADICIONAR
            $('.buy-product-checkbox').live('click', function() {
                //ADICIONANDO PRODUTO NO AMBIENTE
                var rel = $(this).attr('rel');
                if ($(this).is(':checked')) {
                    $.each($('.prateleira-acessorios li .vitrine-acessorios'), function(index, val) {
                        /* iterate through array or object */
                        var itemTitle = $(this).find('.va-titulo');
                        var itemImg = $(this).find('.va-imagem');

                        $(itemTitle).insertBefore(itemImg)
                    });
                    //COMPIANDO INFORMAÃ‡Ã•ES DA VITRINE
                    var titulosAcessorios = $(this).parents('.va-informacoes').parents('li').find('.va-titulo').children('h3').html()
                    var quantidadeAcessorios = $(this).parents('.va-informacoes').find('.product-quantity-value').val();
                    var valorFinalKit = $(this).parents('.va-informacoes').parents('li').find('.va-preco-por').html();
                    var cloneBtn = $(this).parents('.va-informacoes').find('.va-add-remover').html();
                    //MULTIPLICANDO O VALOR DO PRODUTO
                    console.log(valorFinalKit);

                    valorFinalKit = valorFinalKit.replace('R$ ', '').replace('.', '').replace('Por:', '');
                    var valorQuantidadeBuy = parseFloat(valorFinalKit.replace(".", "").replace(",", "."));
                    valorQuantidadeBuy = valorQuantidadeBuy * quantidadeAcessorios;
                    //MOSTRANDO O PRODUTO ADICIONADO COM OS VALORES JÃ MUDADO
                    $('.va-compoem-conteudo #mCSB_1_container').prepend('<div class="box-ambiente" data-rel="' + rel + '"><p>' + titulosAcessorios + '</p><p class="ambiente-quantidade" data-valor="' + valorFinalKit + '">Quantidade: <span class="qtd">' + quantidadeAcessorios + '</span></p><p class="ambiente-valores">' + valorFinalKit + '</p></p><p class="ambiente-valoresQuantidade"><span>' + valorQuantidadeBuy + '</span></p><div class="ambiente-excluir">' + cloneBtn + '</div></div>');
                    alteraValoreAdicionar();
                } else {
                    //REMOVENDO O PRODUTO
                    $.each($('.va-compoem-conteudo').find('[data-rel]'), function() {
                        if ($(this).data('rel') == rel) {
                            $(this).remove();
                        }
                    });
                    mostraValorOriginal();
                }
            });

            //FAZENDO FUNCIONAR O SCROLL
            $(".va-compoem-conteudo").mCustomScrollbar({
                scrollButtons: { enable: true },
                theme: "light-thick",
                scrollbarPosition: "outside"
            });

            //PEGANDO O VALOR DA URL ORIGINAL DO PRODUTO
            //var urlOriginal = $('#BuyButton a').attr('href');
            var urlOriginal = '/checkout/cart/add?';
            //ESCONDENDO BOTÃƒO ORIGINAL
            $('#BuyButton a').addClass('va-hide');
            $('.preco-flutuante .pf-botao-comprar').addClass('va-hide');
            //CLONANDO O BOTÃƒO COMPRAR DO AMBIENTE E COLOCANDO NO INICIO E NA BARRA FLUTUANTE
            var cloneBtnNovo = $('.bt-comprar-kit').clone();
            $('#BuyButton a').before(cloneBtnNovo);
            $('.preco-flutuante .pf-botao-comprar').before(cloneBtnNovo);
            //MONTANDO A URL QUANDO CLICA EM COMPRAR
            $('.bt-comprar-kit a').on('click', function(event) {
                event.preventDefault();
                $.each($('.va-compoem-conteudo .box-ambiente'), function(btnIndex, btnVal) {
                    var btnQuantidade = $(this).find('.qtd').text();
                    var btnId = $(this).attr('data-rel');
                    var btnUrl = '&sku=' + btnId + '&qty=' + btnQuantidade + '&seller=1&redirect=true&sc=1';
                    urlOriginal = urlOriginal + btnUrl;
                    $('.bt-comprar-kit a').attr('href', '' + urlOriginal + '');

                });
                window.location.href = urlOriginal;
            });

            //DEIXANDO A DIV PRODUTOS QUE COMPÃ•EM FIXO QUANDO DAR SCROLL
            var winTop = 0;
            var posicaoDiv = $('.ambiente-flutuante').position().top;
            var deviceResolution = $(window).width();
            if (deviceResolution > 768) {
                $(window).scroll(function() {
                    winTop = $(window).scrollTop();
                    var posicaoDivAtual = $('.ambiente-flutuante').position().top;
                    var ancoraDiv = $('.ancora-final').position().top;
                    $('.va-imagem-destaque').addClass('va-relative');
                    if (winTop >= posicaoDiv && (winTop + $('.ambiente-flutuante').height() - 110) < ancoraDiv) {
                        $('.ambiente-flutuante').addClass('va-flutuante').removeClass('va-flutuante-v2');
                    } else {
                        if (winTop <= posicaoDiv) {
                            $('.box-acessorios').removeClass('va-relative');
                            $('.ambiente-flutuante').removeClass('va-flutuante va-flutuante-v2');
                        } else {
                            $('.box-acessorios').addClass('va-relative');
                            $('.ambiente-flutuante').removeClass('va-flutuante').addClass('va-flutuante-v2');
                        }
                    }
                });
            } else {}
            //MONTANDO O VEJA MAIS IMAGENS
            $.each($('.prateleira-acessorios .va-add-remover'), function(index, val) {
                //PEGANDO O ID DO PRODUTO
                var sku = $(this).find('.buy-product-checkbox').attr('rel');
                //MONSTANDO O CAMINHO AONDE VAI SER COLOCADO AS IMAGENS DA GALERIA
                var local = $(this).parents('.vitrine-acessorios').find('.va-imagem');
                local.append('<div class="modal-galeria" rel="' + sku + '"><div class="overlay"></div><div class="modal-container"><div class="slider-fechar"></div><div class="slider-for"></div><div class="slider-nav"></div></div></div>');
                $.ajax({
                    type: 'GET',
                    url: '/produto/sku/' + sku,
                    dataType: 'json',
                    success: function(data) {
                        var variationsImages = data[0].Images;
                        if (variationsImages.length > 0) {


                            for (i = 0; i < variationsImages.length; i++) {
                                thumbUrl = variationsImages[i][0].Path;
                                var imgSrc = thumbUrl;
                                var zoomImgThumb = '<div><img src="' + imgSrc + '" alt=""></<div>';
                                var imgThumb = '<div><img src="' + imgSrc + '" alt="" width="110" height="110"></<div>';
                                local.find('.slider-for').append(zoomImgThumb);
                                local.find('.slider-nav').append(imgThumb);
                            }
                            local.find('a').addClass('abre-modal');
                            local.append('<a href="#" rel="' + sku + '" class="abre-modal">Clique para ampliar</a>');

                            //ABRINDO O MODAL DA GALERIA
                            $('.abre-modal').bind('click', function(event) {
                                event.preventDefault();
                                $(this).siblings('.modal-galeria').show();
                            });
                        }
                    }
                });
            });

            //FECHANDO O MODAL
            $('.modal-galeria .slider-fechar, .modal-galeria .overlay').on('click', function(event) {
                event.preventDefault();
                $('.modal-galeria').fadeOut();
            });

        }
    },
    valorServico: function() {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? false : sParameterName[1];
                }
            }

        };

        var dev = getUrlParameter('dev');
        /* Se existir GET['dev'] */

        if (dev == 'mN2ULqDp' && $('body').hasClass('produto')) {
            $('body').addClass('mN2ULqDp');
            $('.frete').on('click', '#btnFreteSimulacao', function(event) {
                var referencia = $('#mainContentSide .productReference').html();
                var cep = $('#txtCep').val();
                if (referencia != undefined || referencia != null) {

                    var urlConsulta = 'https://webapi.toqueacampainha.com.br/api/CEP/ConsultaCEPValorServico?cep=' + cep + '&refId=' + referencia + '';
                    $('.box-valor-servico').remove();
                    $.ajax({
                        url: urlConsulta,
                        dataType: 'json',
                        crossDomain: true
                    }).done(function(data) {
                        var verifica = data.VerificaMontagem;
                        var valorServico = data.ValorServico;
                        if (verifica == 1 || valorServico > 0) {
                            var htmlServico = '<div class="box-valor-servico"><p>montagem disponível por <span>R$ ' + valorServico.toFixed(2).replace('.', ',') + '</span></p><p>adicione o serviço de montagem na pagina meu caminhão</p></div>';
                            $('.frete .box-content').append(htmlServico);
                        } else {
                            var htmlServico = '<div class="box-valor-servico texto-diferente"><p>A TOQUE A CAMPAINHA AINDA NÃƒO FAZ MONTAGEM PARA ESTE CEP.</p></div>';
                            $('.frete .box-content').append(htmlServico);
                        }
                    });
                }
            });
        }

    },
    redesignOutlet: function() {
        if ($('body').hasClass('dp-outlet')) {
            var categoriaOutlet = '';
            var menuOutlet = '';
            $.each(dataLayer, function(index, val) {
                if (val.categoryName) {
                    categoriaOutlet = val.categoryName;
                    $.each($('.menu-outline ul li'), function(index, val) {
                        menuOutlet = $(this).attr('data-categoria');
                        if (categoriaOutlet === menuOutlet) {
                            $(this).addClass('outlet-ativo');
                        }
                    });
                }
            });
        }
    },
    productV2: function() {
        if ($('body').hasClass('produto-v2')) {
            //slide na thumb das fotos do produto
            if ($('#productImg ul.thumbs li').length > 5) {
                $('ul.thumbs').slick({
                    slide: 'li',
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    autoplaySpeed: 2000,
                    arrows: true,
                    vertical: true
                });
            }
            //MUDANDO A POSIÃ‡ÃƒO DO CODIGO DO PRODUTO
            var codigoProduct = $('#mainContentSide h1 span').remove();
            $('#mainContentSide h1 .productName').append(codigoProduct);

            setTimeout(function() {

                //REFAZENDO O SELECT DE COR DO PRODUTO
                //AQUI EU TO PEGANDO A PRIMEIRA IMAGEM E COLOCANDO DENTRO DE OUTRA DIV
                if ($('#mainContentSide .componente-tsc').length) {
                    var cloneCorFirst = $('.componente-tsc .grupo-itens div:nth-child(1) a').html();
                    var modeloFirst = '<div class="modeloContent">' + cloneCorFirst + '<span></span></div>';
                    $('#mainContentSide .componente-tsc p').after(modeloFirst);

                }

            }, 100);
            //QUANDO CLICA NA SETA ELE ABRE TODAS AS OPCÃ‡Ã•ES DE CORES
            $(document).on('click', '#mainContentSide .componente-tsc .modeloContent span', function(event) {
                event.preventDefault();
                if ($(this).parent().hasClass('cor-ativada')) {
                    $(this).parent().removeClass('cor-ativada');
                } else {
                    $(this).parent().addClass('cor-ativada');
                }

            });
            //MUDANDO O VALUE DO BOTÃƒO DE CALCULAR O FRETE
            function valueFrete() {
                $('#btnFreteSimulacao').attr('value', 'VERIFICAR');
            };
            var vf = setInterval(function() {
                $('#btnFreteSimulacao').addClass('ok');
                if ($('#btnFreteSimulacao').hasClass('ok')) {
                    valueFrete();
                    clearInterval(vf);
                }

            }, 150);
            //PEGANDO INFORMAÃ‡Ã•ES DA ESPECIFICAÃ‡ÃƒO DO PRODUTO
            var larguraMontato;
            var alturaMontato;
            var profundidadeMontato;
            if ($('.productDescription .Largura-Montado').length) {
                larguraMontato = $('.productDescription .value-field.Largura-Montado').text();
                $('.info-especificacao .ie-largura p span').html(larguraMontato);
            }
            if ($('.productDescription .Altura-Montado').length) {
                alturaMontato = $('.productDescription .value-field.Altura-Montado').text();
                $('.info-especificacao .ie-altura p span').html(alturaMontato);
            }
            if ($('.productDescription .Profundidade-Montado').length) {
                profundidadeMontato = $('.productDescription .value-field.Profundidade-Montado').text();
                $('.info-especificacao .ie-profundidade p span').html(profundidadeMontato);
            }
            //ALTERANDO A POSIÃ‡ÃƒO DA DESCRIÃ‡ÃƒO.
            function contTabela() {
                var conteudoTabela = $('#descriptionWrap .productDescription table').remove();
                $('#descriptionWrap .description-int').prepend(conteudoTabela)
            };
            var ct = setInterval(function() {
                $('#descriptionWrap .productDescription table').addClass('ok');
                if ($('#descriptionWrap .productDescription table').hasClass('ok')) {
                    contTabela();
                    clearInterval(ct);
                }

            }, 150);
            //BARRA DE COMPARTILHAMENTO
            var urlProduto = $(location).attr('href');
            var facebook = '<iframe src="https://www.facebook.com/plugins/like.php?href=' + urlProduto + '&width=172&layout=button_count&action=like&size=small&show_faces=false&share=true&height=46&appId=152175974873349" width="156" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>';
            var twitter = '<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>';
            var googlePLus = '<div class="g-plusone" data-size="medium" data-annotation="inline" data-width="120"></div>';
            var divCompartilhar = '<div class="box-share"><p>Gostou? Então compartilhe com os amigos!</p><div class="share-facebook">' + facebook + '</div><div class="share-twitter">' + twitter + '</div><div class="share-google">' + googlePLus + '</div></div>';
            $('#descriptionWrap .productDescription').append(divCompartilhar);
            //FAZENDO FUNCIONAR VEJA MAIS INFORMAÃ‡Ã•ES
            var posicaoDescricao = $('#descriptionWrap').position().top;
            $('.ancora-descricao p').click(function(event) {
                /* Act on the event */
                $('body,html').animate({
                    scrollTop: posicaoDescricao
                }, 500);
            });
        }
    }

};

var productKit = {
    price: function() {
        var economia = $('.economia-de .economia').html();

        if (!$('.boxBuy').find('.productItem').hasClass('totalKit')) {
            $('.valor-de.price-list-price').hide();
            var valor = $('.valor-de.price-list-price').children('.skuListPrice').html();
            var boleto = '';
            var bol = $('.boleto').children('p').eq(1);
            $('.economia-de').hide();
            var texto = '';
            $.each(bol[0].childNodes, function(ind, val) {
                if ($(val).html() != undefined) {
                    texto = $(val).html();
                    boleto += '<b>' + texto + '</b>';
                } else {
                    texto = $(val).text();
                    boleto += '<span>' + texto + '</span>';
                }
            });
            $('.boxBuy').prepend(
                '<div class="productItem totalKit">' +
                '<div class="info">' +
                '<div class="selection">' +
                '<h4>Total dos produtos</h4>' +
                '</div>' +
                '<div class="discounts" style="display: none"></div>' +
                '<div class="buyLookItem">' +
                '<p class="preco">' + valor + '</p>' +
                '</div>' +
                '</div>' +
                '</div><br />'
            );
            $('.boleto').html(
                '<hr />' +
                '<p class="tit-boleto">Comprando o ambiente completo:</p>' +
                '<p class="precoDisconto"></p>' +
                '<p class="economia">' +
                'Você economiza <b>' + economia + '</b>' +
                '</p>' +
                '<p>Ã vista no boleto por apenas:</p>' +
                '<p>' + boleto + '</p>'
            );
        }
        $('.boleto').find('.economia').html('Você economiza <b>' + economia + '</b>');

    },

    formatMoney: function(num) {
        num = num.split('.');
        var fl = num[1];
        num = num[0];
        var centena = num;
        if (num.length > 3) {
            centena = num.substr(num.length - 3, 3);
            centena = num.substr('-' + num.length - 3, num.length - 3) + '.' + centena;
        }
        return centena + ',' + fl;
    },
    parts: function() {
        if ($('#___rc-p-kit-ids').length > 0) {
            $('.buyLookItem .preco').hide();
            var item = $('#___rc-p-kit-ids').val().split(',');
            if ($('.value-field.Kit-ID-SKU-e-qtd').length > 0) {
                var item_qtd = $('.value-field.Kit-ID-SKU-e-qtd').html().split('|');
                $('.boxBuy').prepend('<div class="SKU-e-qtd" style="display:none;"></div>');
                $.each(item_qtd, function(i, v) {
                    var spl = v.toString().split(',');
                    $('.SKU-e-qtd').append('<div data-sku="' + spl[0].trim() + '" >' + spl[1].trim() + '</div>');
                });

                $('.value-field.Kit-ID-SKU-e-qtd').parent('tr').remove();
                $.each($('.productDescription tr'), function(index, val) {
                    index % 2 == 0 ? $(val).removeClass('even') : $(val).addClass('even');
                });
                $('.boxBuy').prepend(
                    '<table class="kits-extra" cellspacing="0">' +
                    '<tr class="headers">' +
                    '<th class="sku-nome"></th>' +
                    '<th class="sku-qtde">Qtde</th>' +
                    '<th class="sku-unitario">Preço Unitário</th>' +
                    '<th class="sku-total">Sub Total</th>' +
                    '</tr>' +
                    '</table>'
                );
                var _total = 0;
                $.each(item, function(index, val) {
                    var skuData = getSkuData(val);
                    var Qtde = $('[data-sku="' + skuData.id + '"]').html();
                    var price = skuData.price;
                    price = price.toString().split('.');
                    if (price[1] != undefined && price[1].length < 2) {
                        price = price[0] + '.' + price[1] + '0';
                    } else {
                        price = price[0] + '.00';
                    }

                    _total = _total + (price * Qtde);
                    var total = (price * Qtde).toFixed(2);
                    price = price.toString().replace('.', ',');

                    $('.kits-extra').append(
                        '<tr class="contents">' +
                        '<td class="sku-nome">' + skuData.name + '</td>' +
                        '<td class="sku-qtde">' + Qtde + '</td>' +
                        '<td class="sku-unitario">R$ ' + price + '</td>' +
                        '<td class="sku-total">R$ ' + productKit.formatMoney(total) + '</td>' +
                        '</tr>'
                    );
                });
                $('.buyLookItem .preco').show().html('R$ ' + productKit.formatMoney(_total.toFixed(2)));
            }
        }
    },
    call: function() {
        if ($("body").hasClass("kit")) {
            //if ($("body").hasClass("produto") ) {
            productKit.price();
        }
    }
};



var home = {

    getUrlParameter: function(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? false : sParameterName[1];
            }
        }
    },
    bannerTV: function() {
        if ($("#bannerTV").find(".box-banner").length > 0) {
            $("#bannerTV .box-banner").each(function() {
                $(this).attr("class", "royalSlide");
            });

            var defaultSlider = new RoyalSlider("#bannerTV", {
                controlNavThumbs: true,
                // controlNavEnabled: false,
                captionShowEffects: ["moveleft", "fade"]

            });
        }



    },
    finalCountDown: function() {
        if ($('.content-cronometro').find('div').hasClass('contador-ativo')) {
            $('#contador').show();
        }
        $('[data-countdown]').each(function() {
            var $this = $(this),
                finalDate = $(this).data('countdown');
            $this.countdown(finalDate, function(event) {
                var totalHours = event.offset.totalDays * 24 + event.offset.hours;
                var format = totalHours + ':%M:%S';
                $this.html(event.strftime(format));
            });
        });

        $('.btn-confira').click(function(e) {
            e.preventDefault();
            var content = $('#contador');
            var pratelira = $('.produto-cronometro');

            if (content.hasClass('aberto')) {
                content.removeClass('aberto');
                $(this).text('Confira');
                pratelira.fadeOut('fast');
            } else {
                content.addClass('aberto');
                $(this).text('Fechar');
                pratelira.fadeIn('slow');
            }
        })
    },
    sliders: function() {
        if ($('#topsellers>div>ul').find('li').length > 4 || $('#ofertas>div>ul').find('li').length > 4 || $('#lancamentos>div>ul').find('li').length > 4) {
            /*var s = document.getElementsByTagName('link')[3];
            var slicss = document.createElement('link');
            slicss.href = '/arquivos/slick.1.6.css';
            s.parentNode.insertBefore(slicss, s);*/

            $.each($('#topsellers>div>ul'), function(index, val) {
                if ($(this).find('li').length > 4) {
                    $(this).slick({
                        infinite: false,
                        slidesToShow: 4,
                        slidesToScroll: 4,
                        variableWidth: false
                    });
                }

            });
            $.each($('#ofertas>div>ul'), function(index, val) {
                if ($(this).find('li').length > 4) {
                    $(this).slick({
                        infinite: false,
                        slidesToShow: 4,
                        slidesToScroll: 4,
                        variableWidth: false
                    });
                }

            });
            $.each($('#lancamentos>div>ul'), function(index, val) {
                if ($(this).find('li').length > 4) {
                    $(this).slick({
                        infinite: false,
                        slidesToShow: 4,
                        slidesToScroll: 4,
                        variableWidth: false
                    });
                }

            });

        }

    },
    call: function() {
        if ($("body").hasClass("home")) {
            home.bannerTV();
            home.finalCountDown();
            //if( home.getUrlParameter('dev') ) {
            home.sliders();
            //}
        }
    }
}


var department = {
    bannerHead: function() {
        var imgBg = $(".bannerHeadTop img").attr("src");
        $(".bannerHeadTop").css("background-image", "url(" + imgBg + ")");
    },
    advancedSearch: function() {

        var sliderFilters = [];
        $("div#advancedSearch .cont").append("<div class='sliders'></div>");
        $("ul.Altura, ul.Profundidade, ul.Largura").each(function() {
            var array = [];
            var _this = $(this);
            $(this).find("li").each(function() {
                array.push($(this).find("a").attr("title"));
            });

            sliderFilters.push(array);


            function sliderOnChange(e) {
                //$(".filtro_faixa-de-preco label.sr_selected").trigger("click");
                $("div#advancedSearch").find("." + _this.attr("class").split(" ")[0]).attr("data-selected", e.value);
            }


            var rangeArray = array.length - 1;

            var tClass = $(this).attr("class").split(" ")[0];

            var html = '<div class="advFilters filterDrag ' + tClass + '">\
            <h3>' + $(this).attr("class").split(" ")[0] + '</h3>\
            <div id="slider"> \
                 <div id="example" class="k-content">\
                    <div id="climateCtrl">\
                        <div id="rangeslider" class="humidity">\
                            <input />\
                            <input />\
                        </div>\
                    </div>\
                    <p class="selectedLabel"></p>\
                </div>\
            </div></div>';

            $("div#advancedSearch .cont .sliders").append(html);

            ////console.log($("div#advancedSearch").find("."+$(this).attr("class").split(" ")[0]).html());

            function splitValsFirst(val) {
                var n = val.split("a");
                return n[0];
            }

            function splitValsLast(val) {
                var n = val.split("a");
                return n[1];
            }


            $("div#advancedSearch").find("." + tClass).find("#slider").kendoRangeSlider({
                min: 0,
                max: rangeArray,
                smallStep: 1,
                largeStep: 1,
                increaseButtonTitle: splitValsLast(array[rangeArray]),
                decreaseButtonTitle: splitValsFirst(array[0]) + "cm",
                showButtons: true,
                slide: function(e) {
                    e.sender.options.tooltip.format = splitValsFirst(array[e.value[0]]) + " até " + splitValsLast(array[e.value[1]]);
                },
                change: sliderOnChange
            });

        });


        html = '<div class="advFilters faixaPreco">\
            <h3>Faixa de Preço</h3>\
            <ul class="faixaPrecoWrap">\
            ' + $("ul.Faixa.de.preço").html() + '\
            </ul>\
        </div>';
        $("div#advancedSearch .cont").append(html);

        html = '<div class="advFilters adColor">\
            <h3>Cor</h3>\
            <ul class="adColorWrap">\
            ' + $("ul.Cor").html() + '\
            </ul>\
        </div>';
        $("div#advancedSearch .cont").append(html);

        $("div#advancedSearch").on("click", "a", function(event) {
            event.preventDefault();

            var aFp = "";
            $(this).toggleClass("active");

            $(this).closest("div").find("li").each(function(i) {
                if ($(this).find("a").hasClass("active")) {
                    var sel = $(this).find("a").attr("href");
                    var b = sel.replace(window.location.origin + window.location.pathname, "")
                    b = b.split("?");
                    aFp += b[0] + "#";

                }
            });
            $(this).closest("div").attr("data-selected", aFp);

        });


        $("#advancedSearch a.butBlue").on("click", function(event) {
            event.preventDefault();
            var url = "/";

            switch (window.location.pathname) {
                case '/sala-de-estar':
                    url = "/sofa";
                    break;
                case '/quarto':
                    url = "/guarda-roupa---armario";
                    break;
                case '/sala-de-jantar':
                    url = "/Mesa-de-jantar";
                    break;
                case '/escritorio':
                    url = "/Escrivaninhas";
                    break;
                case '/copa-e-cozinha':
                    url = "/Mesas-de-Copa-e-Cozinha";
                    break;
                case '/varanda-e-jardim':
                    url = "/Mesa";
                    break;
                case '/infantil':
                    url = "/berço";
                    break;
            }


            var map = "?map=c,c";
            $("#advancedSearch .advFilters").each(function() {
                var _this = $(this);

                if (typeof _this.attr("data-selected") != "undefined") {

                    if (_this.attr("class").indexOf("Altura") > 0) {
                        //fq=specificationFilter_37:PP06+(1.30+-+1.35mm)&fq=specificationFilter_37:PP08+(1.40+-+1.50mm)
                        var array = [];
                        $("ul.Altura li").each(function() {
                            array.push($(this).find("a").attr("title"));
                        });

                        var arrayRange = _this.attr("data-selected");
                        var arrayRange1 = parseInt(arrayRange.split(",")[0]);
                        var arrayRange2 = parseInt(arrayRange.split(",")[1]);

                        for (var i = arrayRange1; i <= arrayRange2; i++) {
                            url += "/" + array[i];
                            map += ",specificationFilter_87";
                        }
                    }
                    if (_this.attr("class").indexOf("Largura") > 0) {
                        var array = [];
                        $("ul.Largura li").each(function() {
                            array.push($(this).find("a").attr("title"));
                        });

                        var arrayRange = _this.attr("data-selected");
                        var arrayRange1 = parseInt(arrayRange.split(",")[0]);
                        var arrayRange2 = parseInt(arrayRange.split(",")[1]);


                        for (var i = arrayRange1; i <= arrayRange2; i++) {
                            url += "/" + array[i];
                            map += ",specificationFilter_88";
                        }


                    }
                    if (_this.attr("class").indexOf("Profundidade") > 0) {

                        var array = [];
                        $("ul.Profundidade li").each(function() {

                            array.push($(this).find("a").attr("title"));
                        });

                        var arrayRange = _this.attr("data-selected");
                        var arrayRange1 = parseInt(arrayRange.split(",")[0]);
                        var arrayRange2 = parseInt(arrayRange.split(",")[1]);

                        for (var i = arrayRange1; i <= arrayRange2; i++) {
                            url += "/" + array[i];
                            map += ",specificationFilter_89";
                        }

                    }
                    if (_this.attr("class").indexOf("adColor") > 0) {
                        var tFp = _this.attr("data-selected");
                        tFp = tFp.split("#");
                        for (var i = 0; i < tFp.length - 1; i++) {
                            url += tFp[i];
                            map += ",specificationFilter_96";
                        }
                    }
                    if (_this.attr("class").indexOf("faixaPreco") > 0) {
                        var tFp = _this.attr("data-selected");
                        tFp = tFp.split("#");
                        for (var i = 0; i < tFp.length - 1; i++) {
                            url += tFp[i];
                            map += ",priceFrom";
                        }


                    }

                }
            });
            ////console.log(url+map);
            window.location = window.location.pathname + url + map;
        });

        $('ul.adColorWrap a').each(function() {
            var _this = $(this);
            var titleAttr = _this.attr('title');
            _this.addClass(titleAttr);
        });

    },

    advancedSearch2: function() {

        $("#advancedSearch .cont > div").show();
        if (window.location.pathname == "/promocao") {
            $("#advancedSearch").remove();
        }
        if (window.location.pathname.indexOf('/campanha') == 0 && $('body').hasClass('campanha')) {
            //$(".resultItemsWrapper").prepend('<h2 class="titulo-campanha">Promocoes</h2>');
            //$('<h2 class="titulo-campanha">Promocoes</h2>').insertBefore(".resultItemsWrapper");
        }

        $("#advancedSearch").find(".Altura, .Profundidade, .Largura").each(function() {
            var array = [];
            var _this = $(this);
            _this.find("input").each(function() {
                array.push($(this).attr("data-name"));
            });

            function sliderOnChange(e) {
                //$(".filtro_faixa-de-preco label.sr_selected").trigger("click");
                //console.log(e);
                //console.log(_this.attr("class"));
                _this.attr("data-selected", e.value);
            }

            function splitValsFirst(val) {
                var n = val.split("a");
                return n[0];
            }

            function splitValsLast(val) {
                var n = val.split("a");
                return n[1];
            }

            var rangeArray = array.length - 1;

            //console.log(array);

            //console.log(rangeArray)

            if (rangeArray > 0) {

                _this.find("#slider").kendoRangeSlider({
                    min: 0,
                    max: rangeArray,
                    smallStep: 1,
                    largeStep: 1,
                    increaseButtonTitle: splitValsLast(array[rangeArray]),
                    decreaseButtonTitle: splitValsFirst(array[0]) + "cm",
                    showButtons: true,
                    slide: function(e) {
                        e.sender.options.tooltip.format = splitValsFirst(array[e.value[0]]) + "cm até " + splitValsLast(array[e.value[1]]);
                    },
                    change: sliderOnChange
                });
            } else {
                _this.attr("data-selected", "0,0");
                _this.find("#slider").append("<p>" + _this.attr("class").split(" ")[2] + " único(a): " + array + "</p>").show();
                _this.find("#slider > div").hide();
            }
        });

        $("div#advancedSearch").on("click", "a", function(event) {
            event.preventDefault();

            var aFp = "";
            $(this).toggleClass("active");

            $(this).closest(".advFilters").find("li").each(function(i) {
                if ($(this).find("a").hasClass("active")) {
                    var sel = $(this).find("a").attr("href");
                    var b = sel.replace(window.location.origin + window.location.pathname, "")
                    b = b.split("/");
                    b = b[3].split("?");
                    aFp += b[0] + "#";

                }
            });
            $(this).closest(".advFilters").attr("data-selected", aFp);

        });

        $('ul.adColorWrap a').each(function() {
            var _this = $(this);
            var titleAttr = _this.attr('title');
            _this.addClass(titleAttr);
        });

        $("#advancedSearch a.butBlue").on("click", function(event) {
            event.preventDefault();
            var url = "/";

            switch (window.location.pathname) {
                case '/sala-de-estar':
                    url = "/sofa";
                    break;
                case '/quarto':
                    url = "/guarda-roupa---armario";
                    break;
                case '/sala-de-jantar':
                    url = "/Mesa-de-jantar";
                    break;
                case '/escritorio':
                    url = "/Escrivaninhas";
                    break;
                case '/copa-e-cozinha':
                    url = "/Mesas-de-Copa-e-Cozinha";
                    break;
                case '/varanda-e-jardim':
                    url = "/Mesa";
                    break;
                case '/infantil':
                    url = "/berço";
                    break;
            }


            var map = "?map=c,c";
            $("#advancedSearch .advFilters").each(function() {
                var _this = $(this);

                if (typeof _this.attr("data-selected") != "undefined") {

                    if (_this.attr("class").indexOf("Altura") > 0) {
                        //fq=specificationFilter_37:PP06+(1.30+-+1.35mm)&fq=specificationFilter_37:PP08+(1.40+-+1.50mm)
                        var array = [];
                        _this.find("input").each(function() {
                            array.push($(this).attr("data-name"));
                        });

                        var arrayRange = _this.attr("data-selected");
                        var arrayRange1 = parseInt(arrayRange.split(",")[0]);
                        var arrayRange2 = parseInt(arrayRange.split(",")[1]);

                        for (var i = arrayRange1; i <= arrayRange2; i++) {
                            url += "/" + array[i];
                            map += ",specificationFilter_87";
                        }
                    }
                    if (_this.attr("class").indexOf("Largura") > 0) {
                        var array = [];
                        _this.find("input").each(function() {
                            array.push($(this).attr("data-name"));
                        });

                        var arrayRange = _this.attr("data-selected");
                        var arrayRange1 = parseInt(arrayRange.split(",")[0]);
                        var arrayRange2 = parseInt(arrayRange.split(",")[1]);


                        for (var i = arrayRange1; i <= arrayRange2; i++) {
                            url += "/" + array[i];
                            map += ",specificationFilter_88";
                        }


                    }
                    if (_this.attr("class").indexOf("Profundidade") > 0) {

                        var array = [];
                        _this.find("input").each(function() {

                            array.push($(this).attr("data-name"));
                        });

                        var arrayRange = _this.attr("data-selected");
                        var arrayRange1 = parseInt(arrayRange.split(",")[0]);
                        var arrayRange2 = parseInt(arrayRange.split(",")[1]);

                        for (var i = arrayRange1; i <= arrayRange2; i++) {
                            url += "/" + array[i];
                            map += ",specificationFilter_89";
                        }

                    }

                    //console.log(_this.attr("class"));
                    if (_this.attr("class").indexOf("adColor") > 0) {
                        var tFp = _this.attr("data-selected");
                        tFp = tFp.split("#");
                        for (var i = 0; i < tFp.length - 1; i++) {
                            url += "/" + tFp[i];
                            map += ",specificationFilter_96";
                        }
                    }
                    if (_this.attr("class").indexOf("faixaPreco") > 0) {
                        var tFp = _this.attr("data-selected");

                        tFp = tFp.split("#");
                        for (var i = 0; i < tFp.length - 1; i++) {
                            url += "/" + tFp[i];
                            map += ",priceFrom";
                        }


                    }

                }
            });
            //console.log(window.location.pathname + url+map);
            //window.location = window.location.pathname+url+map;
        });
    },
    slider: function() {

        if ($('body').hasClass('promocoes')) {

            $.each($('.prateleira>ul'), function(index, val) {

                if ($(this).find('li').length > 4) {
                    $(this).slick({
                        infinite: true,
                        slidesToShow: 4,
                        slidesToScroll: 4,
                        //variableWidth: false,
                        responsive: [
                            {
                              breakpoint: 1024,
                              settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3,
                                infinite: true
                              }
                            },
                            {
                              breakpoint: 600,
                              settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2
                              }
                            },
                            {
                              breakpoint: 480,
                              settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                              }
                            }
                        ]
                    });
                }

            });

        }

    },
    ambientes: function() {
        for (var i = 0; i < dataLayer.length; i++) {
            if (dataLayer[i].pageDepartment == "Ambientes") {
                $('#mainContentSide').addClass('nohover');
                i = dataLayer.length;
            }
        }
    },
    call: function() {
        if ($("body").hasClass("departamento")) {
            //department.bannerHead();
            department.advancedSearch();
            department.advancedSearch2();
            department.slider();
            department.ambientes();
        }
    }
}


var category = {
    smtResearch: function() {
        $("#smtRsc input[type='checkbox']").vtexSmartResearch({
            callback: function() {


            },
            shelfCallback: function() {


            }
        });
    },
    openFilter: function() {
        $(".search-multiple-navigator > h4").on("click", function(event) {
            event.preventDefault();
            var that = $(this);
            that.next("ul").slideToggle(400);

        });

        if ($(".search-multiple-navigator > h4").next("ul").length < 1) {
            $(".search-multiple-navigator > h4").remove();
        }

        $(".search-multiple-navigator h5").on("click", function() {
            var that = $(this);

            that.closest("fieldset").find("> div").slideToggle(400);
            that.closest("fieldset").toggleClass("inactive");
        });

        setTimeout(function() {
            $('#sizeFilters fieldset').addClass('inactive');
            $('#sizeFilters fieldset').find("> div").hide();
        }, 300);


    },
    openFilterSearch: function() {
        $(".search-single-navigator > h3").addClass("inactive");
        $(".search-single-navigator > h3").on("click", function(event) {
            event.preventDefault();
            var that = $(this);
            that.next("ul").slideToggle(400);

        });

        $(".search-single-navigator > ul").each(function() {
            if ($(this).find("li").length < 1) {
                $(this).prev("h3").remove();
                $(this).remove();
            };
        })

        $(".search-single-navigator h3").on("click", function() {
            var that = $(this);
            that.toggleClass("inactive");
        });
    },
    specialFilters: function() {
        $("<div id='sizeFilters'></div>").insertAfter(".refino-marca");
        var filterToSpecial = [
            "filtro_tamanho",
            "filtro_macio-ou-firme?",
            "filtro_com-o-tecido-certo",
            "filtro_para-que?",
            "filtro_use-tambem-para",
            "filtro_design",
            "filtro_gavetas",
            "filtro_genero",
            "filtro_utilidades",
            "filtro_qualidades",
            "filtro_somos-um-casal",
            "filtro_praticidade",
            "filtro_onde-usar?",
            "filtro_pra-que?",
            "filtro_o-que-tem?",
            "filtro_encosto",
            "filtro_assento"
        ];
        var filterToSize = ["filtro_altura", "filtro_largura", "filtro_profundidade"];
        $(".search-multiple-navigator fieldset").each(function() {
            _this = $(this);
            $.each(filterToSpecial, function(i) {
                if (_this.attr("class").indexOf(filterToSpecial[i]) > 0) {
                    var filterClass = _this.attr("class");
                    var filterNewClass = filterClass.replace("?", "")
                    _this.attr("class", filterNewClass);
                    _this.addClass("specialFilter");
                    cDiv = _this.closest("div");
                    _this.prependTo(cDiv);
                    _this.parent().append(_this.appendTo(cDiv).remove());
                    return false;
                }
            });
            $.each(filterToSize, function(i) {
                if (_this.attr("class").indexOf(filterToSize[i]) > 0) {
                    _this.prependTo("#sizeFilters");
                    return false;
                }
            });
        });
    },
    orderBy: function(filtro) {
        // Criar as tags "a" dos Filtros "Preco" e "Mais Vendidos";

        var maisVendidos = $("<a>").attr({
            href: "",
            "class": "mais-vendidos"
        }).text("Mais Vendidos");
        var menorPreco = $("<a>").attr({
            href: "",
            "class": "menor-preco"
        }).text("Menor Preço");
        var maiorPreco = $("<a>").attr({
            href: "",
            "class": "maior-preco"
        }).text("Maior Preço");
        var ordemLancamento = $("<a>").attr({
            href: "",
            "class": "ordem-lancamento"
        }).text("Lançamento");

        // Acrescentar as tags "a" no documento
        // Adicionar os links nas tags "a"
        $(filtro).append("<div class='orderBy'><p>Ordenar por:</p></div>")
        $('.orderBy').append(ordemLancamento).append(maiorPreco).append(menorPreco).append(maisVendidos);
        var linkFiltro = $(filtro).children('.orderBy').find("a");
        linkFiltro.bind("click", function(e) {

            var urlOrigin = 'http://' + document.location.hostname;

            var canonical = $("link[rel=canonical]").attr("href").replace(urlOrigin, "");
            var canonicalWithout = $("link[rel=canonical]").attr("href").replace(urlOrigin, "");
            // Primeira letra maiúscula
            function capitaliseFirstLetter(string) {
                return string.charAt(0).toUpperCase() + string.slice(1);
            }

            e.preventDefault();
            var reMap = /(map)/g;

            var searchReplace = location.search.replace('&O=OrderByPriceASC', '').replace('&O=OrderByPriceDESC', '').replace('&O=OrderByReleaseDateDESC', '').replace('&O=OrderByTopSaleDESC', '');

            if ($(this).hasClass("menor-preco")) {
                if (reMap.test(urlFiltro)) {
                    if (searchReplace != "") {
                        window.location.href = urlOrigin + "/" + capitaliseFirstLetter(canonicalWithout) + searchReplace + '&O=OrderByPriceASC';
                    } else {
                        window.location.href = urlOrigin + "/" + capitaliseFirstLetter(canonicalWithout) + '?PS=12&map=specificationFilter_18&O=OrderByPriceASC';
                    }
                } else {
                    if (parseInt(canonical.replace('/', '')) > 0) {
                        window.location.href = "/busca/?fq=H:" + parseInt(canonical.replace('/', '')) + '&PS=12&O=OrderByPriceASC';
                    } else {
                        window.location.href = canonical + '?PS=12&O=OrderByPriceASC';
                    }
                }
            } else if ($(this).hasClass("maior-preco")) {
                if (reMap.test(urlFiltro)) {
                    if (searchReplace != "") {
                        window.location.href = urlOrigin + "/" + capitaliseFirstLetter(canonicalWithout) + searchReplace + '&O=OrderByPriceDESC';
                    } else {
                        window.location.href = urlOrigin + "/" + capitaliseFirstLetter(canonicalWithout) + '?PS=12&map=specificationFilter_18&O=OrderByPriceDESC';
                    }
                } else {
                    if (parseInt(canonical.replace('/', '')) > 0) {
                        window.location.href = "/busca/?fq=H:" + parseInt(canonical.replace('/', '')) + '&PS=12&O=OrderByPriceDESC';
                    } else {
                        window.location.href = canonical + '?PS=12&O=OrderByPriceDESC';
                    }
                }
            } else if ($(this).hasClass("ordem-lancamento")) {
                if (reMap.test(urlFiltro)) {
                    if (searchReplace != "") {
                        window.location.href = urlOrigin + "/" + capitaliseFirstLetter(canonicalWithout) + searchReplace + '&O=OrderByReleaseDateDESC';
                    } else {
                        window.location.href = urlOrigin + "/" + capitaliseFirstLetter(canonicalWithout) + '?PS=12&map=specificationFilter_18&O=OrderByReleaseDateDESC';
                    }
                } else {
                    if (parseInt(canonical.replace('/', '')) > 0) {
                        window.location.href = "/busca/?fq=H:" + parseInt(canonical.replace('/', '')) + '&PS=12&O=OrderByReleaseDateDESC';
                    } else {
                        window.location.href = canonical + '?PS=12&O=OrderByReleaseDateDESC';
                    }
                }
            } else if ($(this).hasClass("mais-vendidos")) {
                if (reMap.test(urlFiltro)) {
                    if (searchReplace != "") {
                        window.location.href = urlOrigin + "/" + capitaliseFirstLetter(canonicalWithout) + searchReplace + '&O=OrderByTopSaleDESC';
                    } else {
                        window.location.href = urlOrigin + "/" + capitaliseFirstLetter(canonicalWithout) + '?PS=12&map=specificationFilter_18&O=OrderByTopSaleDESC';
                    }
                } else {
                    if (parseInt(canonical.replace('/', '')) > 0) {
                        window.location.href = "/busca/?fq=H:" + parseInt(canonical.replace('/', '')) + '&PS=12&O=OrderByTopSaleDESC';
                    } else {
                        window.location.href = canonical + '?PS=12&O=OrderByTopSaleDESC';
                    }
                }
            };
        });

        // Adicionar a cor nas tags "a" de acordo com a página
        var urlFiltro = window.location.href;
        var reMinPreco = /(PriceASC)/g;
        var reMaxPreco = /(PriceDESC)/g;
        var reNew = /(Release)/g;
        var reVendas = /(TopSale)/g;
        if (reMinPreco.test(urlFiltro)) {
            $(".orderBy .menor-preco").toggleClass("active");
        } else if (reMaxPreco.test(urlFiltro)) {
            $(".orderBy .maior-preco").toggleClass("active");
        } else if (reNew.test(urlFiltro)) {
            $(".orderBy .ordem-lancamento").toggleClass("active");
        } else if (reVendas.test(urlFiltro)) {
            $(".orderBy .mais-vendidos").toggleClass("active");
        };
    },
    searchResult: function() {
        // Página de busca
        var pagina_busca = $('.resultado-busca'),
            busca_vazia = $('.busca-vazio'),
            searchNavigator = $('#searchNavigator'),
            pagina_produto = $('#produto'),
            quantidade = $('.quantidade');

        /* Página de busca vazia*/
        if (pagina_busca[0]) {
            var url = window.location.toString().split('/'),
                split_ft = url[3].split('?'),
                termo = split_ft[0];
            pagina_busca.find('#busca-vazia strong').html(decodeURIComponent(termo));
            if (busca_vazia[0]) {
                pagina_busca.addClass('nenhum-resultado');
            } else {
                // var valorResult = $('.resultado-busca-termo:eq(0)').find('.value').text();
                $(".resultado-busca-termo").find(".value").text(decodeURIComponent(termo));
                pagina_busca.addClass('resultado-encontrado');
            };
        };
    },
    resultTime: function() {
        var resultTopTime = $('p.searchResultsTime').eq(0).html();
        $('#catHeader').html(resultTopTime);
    },
    colecoes: function() {
        $('#catHeader .resultado-busca-numero , #catHeader .resultado-busca-termo , #catHeader .resultado-busca-tempo').remove();
        var nomeColecao = $('#nome-colecao').text();
        $('#catHeader').prepend('<h2 class="titulo-sessao">' + nomeColecao + '</h2>');
    },
    inverterFiltro: function() {
        var filtro = $('.search-single-navigator').clone();
        $('.menu-navegue').after(filtro);
        $('.search-multiple-navigator').next('.search-single-navigator').remove();
        $('.search-single-navigator').show();
    },
    ambientes: function() {
        for (var i = 0; i < dataLayer.length; i++) {
            if (dataLayer[i].pageDepartment == "Ambientes") {
                $('#mainContentSide').addClass('nohover');
                i = dataLayer.length;
            }
        }
    },
    call: function() {
        if ($('body').hasClass('categoria') || $('body').hasClass('catalogo')) {
            category.smtResearch();
            category.openFilter();
            category.specialFilters();
            category.searchResult();
            category.orderBy('#catHeader');
            if ($('body').hasClass('catalogo')) {
                category.inverterFiltro();
            }

        } else if ($("body").hasClass("resultado-busca")) {
            category.openFilter();
            category.openFilterSearch();
            category.searchResult();
            category.resultTime();
            category.orderBy('#catHeader');
            if ($("body").hasClass("resultado-encontrado")) {
                category.smtResearch();
                if ($("body").hasClass("layout-colecoes")) {
                    category.colecoes();
                };
            };

        };

        category.ambientes();
    }


}

var institucional = {
    menuLateral: function() {
        var navLink = $('.institucional-nav a');

        navLink.each(function() {
            var url = document.location.pathname.split('/')[2];
            if ($(this).attr('class') === url) {

                $(this).addClass('active');
            };
        })
    },
    call: function() {
        if ($('body').hasClass('institucional')) {
            institucional.menuLateral();
        };
    }
}
var cupom = {
    cupomGrid: function() {
        $('.cupom-grid').find('a').attr('target', '_blank');
    },
    newsletter: function() {
        $('.newsletter-cupom').append('<div class="menssagem"></div>');
        $('#ok').live('click', function() {
            var dados = {};

            dados.nome = $('#nome').val();
            dados.email = $('#email').val();
            $.ajax({
                accept: 'application/vnd.vtex.ds.v10+json',
                contentType: 'application/json; charset=utf-8',
                crossDomain: true,
                data: JSON.stringify(dados),
                type: 'PATCH',
                url: '//api.vtexcrm.com.br/toqueacampainha/dataentities/CN/documents',
                success: function(data) {
                    $('.newsletter-cupom').find('.menssagem').html(data);
                },
                error: function(error) {
                    console.log(error);
                }
            });

        });
    },
    call: function() {
        if ($("body").hasClass("cupom-responsive")) {
            cupom.cupomGrid();
            cupom.newsletter();
        }
    }
}

$(document).ready(function() {
    geral.call();
    home.call();
    product.call();
    product.kitAcessorios();
    product.valorServico();
    product.redesignOutlet();
    product.productV2();
    productKit.call();
    productKit.parts();
    department.call();
    category.call();
    institucional.call();
    cupom.call();

    // forçar carrinho a ficar escondido ao carregar a página .portal-minicart-ref{display: none;}
    setTimeout(function() {
        $(".portal-minicart-ref").show();
    }, 5000);


    $('.grayBox.mostruario a').on('click', function(event) {
        event.preventDefault();
        product.mostruario();
        product.modalExibir()
    });
    $('.info-close, .overlayMostruario').on('click', function(event) {
        event.preventDefault();
        product.modalOcultar()
    });
    $('[href="/institucional/entrega-e-montagem"]').on('click', function(event) {
        event.preventDefault();
        product.montagemGratis();
        product.modalExibir()
    });

    // NoFollow
    $('.search-single-navigator h5+ul').find('a').attr('rel', 'nofollow');
});

$(document).one('ajaxStop', function() {
    //product.kitAcessoriosParcela();
});
$(document).ajaxStop(function() {
    if ($("body").hasClass("produto")) {
        product.list();
        product.boleto();
        product.parcelamento();
        //product.montagem();
        product.buyTogether();

        productKit.call();
    };
    if ($("body").hasClass("resultado-busca")) {
        category.specialFilters();
    };
    geral.tabs();

    // Inserir link no carrinho
    var addButtonCart = function() {
        var linkCarrinho = $("<a>").attr({
            "class": "linkCarrinho-cart",
            href: "/checkout/#/cart"
        }).text("ver meu caminhão");
        if ($(".linkCarrinho-cart").length < 1) {
            linkCarrinho.insertAfter(".cartCheckout");
        };
    };
    addButtonCart();

    var lineCarrinho = function() {
        var qntItensCart = parseFloat($(".amount-items-em").text());
        if (qntItensCart !== 0) {
            $("#header .cart").addClass('acti-hover');
        };
    };
    lineCarrinho();

    // Carrossel carrinho
    var carouselCartVert = function() {
        var numItens = $(".vtexsc-productList").find("tr").length - 1;
        var buttonTop = $("<span>").addClass("button-top");
        var buttonBot = $("<span>").addClass("button-bot");
        var tableCart = $(".vtexsc-productList");
        var cartWrap = $(".vtexsc-wrap");
        cartWrap.after(buttonTop).after(buttonBot);
        var hItem = 106; // altura das tr's

        buttonTop.on("click", function() {
            slide = tableCart.position().top;
            pos_slide = (slide - hItem);

            if (numItens > 2) {
                if (slide <= (-hItem * (numItens - 1)) + 106) {
                    tableCart.animate({
                        top: slide + "px"
                    });

                } else {
                    // //console.log(pos_slide)
                    tableCart.animate({
                        top: (pos_slide) + "px"
                    });
                }
            };
        });

        buttonBot.on("click", function() {
            slide = tableCart.position().top;
            pos_slide = (slide + hItem);
            if (slide >= 0) {
                tableCart.animate({
                    top: "0px"
                });
            } else {
                tableCart.animate({
                    top: (pos_slide) + "px"
                });
            }
        });

        if (numItens <= 2) {
            buttonTop.hide();
            buttonBot.hide();
        };

        $(".cart").mouseover(function() {
            if ($('.vtexsc-cart').is(':visible')) {
                $(".cart").addClass("active");
            }
        })
        $(".cart").mouseout(function() {
            setTimeout(function() {
                if (!$('.vtexsc-cart').is(':visible')) {
                    $(".cart").removeClass("active");
                }
            }, 800);
        })
    };
    carouselCartVert();

});
$(window).load(function() {
    if ($('body').hasClass('produto-acessorios')) {
        //DEIXANDO TODOS OS PRODUTOS ADICIONADO
        $.each($('.prateleira-acessorios fieldset'), function(index, val) {
            /* iterate through array or object */
            $(this).find('.buy-product-checkbox').prop('checked', true).trigger('click');
        });



        //FUNCIONANDO O CARROUSEL DA GALERIA
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true,
            infinite: false,
            asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: true,
            centerMode: true,
            focusOnSelect: true
        });

    }
});
